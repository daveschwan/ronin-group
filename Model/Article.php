<?php
class Article extends AppModel {







/**
 * Gets a specicfic article
 * @param  array  $options [description]
 * @return [type]          [description]
 */
	public function getArticle($options = array()) {

		$defaults = array(
			'id' => null, // method to find article
			'slug' => null,
			'activeOnly' => true,
		);

		extract(array_merge($defaults, $options));

		if(empty($id) && empty($email)) {
			return false;
		}

		$params = array(
			'conditions' => array(
				'NOT' => array(
					'status' => 3
				)
			),
		);

		if(!empty($activeOnly)) {
			$params['conditions'][$this->alias . '.active'] = true;
		}

		if(!empty($id)) {
			$params['conditions'][$this->alias . '.id'] = $id;
		}

		if(!empty($slug)) {
			$params['conditions'][$this->alias . '.slug'] = $slug;
		}

		$article = $this->find('first', $params);
		if(empty($article)) {
			return false;
		}
		return $article;

	}




/**
 * Gets an array of articles
 * @return [type] [description]
 */
	public function getArticles($opts = array()) {

		$defaults = array(
			'activeOnly' => true,
			'limit' => 20,
			'findType' => 'all'
		);
		$params = extract(array_merge($defaults, $opts));
		$query = array(
			'conditions' => array(
				'NOT' => array(
					'status' => '3'
				)
			),
			'limit' => $limit,
			'order' => 'Article.name ASC',
		);

		//active only
		if(!empty($activeOnly)) {
			$query['conditions'][$this->alias . '.active'] = 1;
		}

		//limit
		if(!empty($limit)) {
			$query['limit'] = $limit;
		}

		if(!empty($paginate)) {
			return $query;
		} else {
			$articles = $this->find($findType, $query);
			return $articles;
		}
	}



/**
 * [add description]
 * @param [type] $data [description]
 */
	public function add($data) {
		if(empty($data)) {
			return false;
		}

		//creates the new article
		$article = $this->saveAll($data);
		return $article;
	}




/**
 * Edits an existing article's account info (not their profile-info)
 * @return [type] [description]
 */
	public function edit($data) {
		if($this->save($data)) {
			return true;
		}
		return false;
	}



/**
 * Sets a status to 'deleted'
 * @param  char(36) - $id - id of a article
 * @return [type]     [description]
 */
	public function deleteArticle($id) {
		if(empty($id)) {
			return false;
		}
		$this->id = $id;
		$this->saveField('status', 3);
		return true;
	}



}