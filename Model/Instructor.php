<?php
class Instructor extends AppModel {


public $hasMany = array(
	'Photo' => array(
		'foreignKey' => 'foreign_key',
		'conditions' => array(
			'Photo.model' => 'Instructor',
		)
	)
);

public $hasAndBelongsToMany = array(
	'CourseInstance'
);











/**
 * Gets a specicfic instructor
 * @param  array  $options [description]
 * @return [type]          [description]
 */
	public function getInstructor($id, $options = array()) {

		if(empty($id)) {
			return false;
		}

		$params = array(
			'conditions' => array(
				$this->alias . '.id' => $id
			),
			'contain' => array(
				'CourseInstance'
			)
		);

		$instructor = $this->find('first', $params);
		if(empty($instructor)) {
			return false;
		}
		return $instructor;

	}




/**
 * Gets an array of courses
 * @return [type] [description]
 */
	public function getInstructors($opts = array()) {

		$defaults = array(
			'limit' => 20,
			'findType' => 'all'
		);
		$params = extract(array_merge($defaults, $opts));
		$query = array(
			'conditions' => array(),
			'contain' => array(
				'CourseInstance'
			),
			'limit' => $limit,
			'order' => 'Instructor.name ASC',
		);

		//limit
		if(!empty($limit)) {
			$query['limit'] = $limit;
		}

		if(!empty($paginate)) {
			return $query;
		} else {
			$instructors = $this->find($findType, $query);
			return $instructors;
		}
	}





/**
 * Adds a new instructor
 * @param [type] $data [description]
 */
	public function add($data) {
		if(empty($data)) {
			return false;
		}
		$instructor = $this->saveAll($data);
		return $instructor;
	}




/**
 * Edits an existing instructor
 * @return [type] [description]
 */
	public function edit($data) {
		if($this->save($data)) {
			return true;
		}
		return false;
	}





/**
 * Sets a status to 'deleted'
 * @param  char(36) - $id - id of a course
 * @return [type]     [description]
 */
	public function deleteInstructor($id) {
		if(empty($id)) {
			return false;
		}
		return $this->delete($id);
	}

}