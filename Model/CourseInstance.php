<?php
class CourseInstance extends AppModel {

	public $actsAs = array(
		'Time' => array(
			'timeFields' => array(
				'start',
				'end',
				// 'repeat_until'
			)
		),
	);

	public $belongsTo = array(
		'Course' => array(
			'counterCache' => true,
			'counterScope' => array(
				'CourseInstance.status' => array(1,2)
			)
		)
	);

	public $hasMany = array(
		'Attendee',
	);

	public $hasAndBelongsToMany = array(
		'Instructor'
	);



	public $validate = array(
		'location' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'last' => true,
				'message' => 'Must enter a location.'
			)
		)
	);







/**
 * Gets a specicfic course instance
 * @param  array  $options [description]
 * @return [type]          [description]
 */
	public function getCourseInstance($id, $options = array()) {

		$defaults = array(
			'activeOnly' => true,
		);

		extract(array_merge($defaults, $options));

		if(empty($id)) {
			return false;
		}

		$params = array(
			'conditions' => array(
				$this->alias . '.status' => 1
			),
			'contain' => array(
				'Course'
			)
		);

		if(!empty($activeOnly)) {
			$params['conditions'][$this->alias . '.active'] = true;
		}

		if(!empty($id)) {
			$params['conditions'][$this->alias . '.id'] = $id;
		}

		$courseInstance = $this->find('first', $params);
		if(empty($courseInstance)) {
			return false;
		}
		return $courseInstance;

	}




/**
 * Gets an array of course instances
 * @return [type] [description]
 */
	public function getCourseInstances($courseId, $opts = array()) {

		if(empty($courseId)) {
			return false;
		}

		$defaults = array(
			'activeOnly' => true,
			'limit' => 20,
			'findType' => 'all',
			'paginate' => false,
		);
		extract(array_merge($defaults, $opts));

		$query = array(
			'conditions' => array(
				'course_id' => $courseId,
				'status' => 1
			),
			'limit' => $limit,
			'order' => 'CourseInstance.start_utc ASC',
		);

		//active only
		if(!empty($activeOnly)) {
			$query['conditions'][$this->alias . '.active'] = 1;
		}

		//limit
		if(!empty($limit)) {
			$query['limit'] = $limit;
		}

		if(!empty($paginate)) {
			return $query;
		} else {
			$attendees = $this->find($findType, $query);
			return $attendees;
		}
	}



/**
 * Adds a new course instance
 * @param [type] $data [description]
 */
	public function add($data) {
		if(empty($data)) {
			return false;
		}
		$courseInstance = $this->saveAll($data);
		return $courseInstance;
	}




/**
 * Edits an existing course instance
 * @return [type] [description]
 */
	public function edit($data) {
		if($this->save($data)) {
			return true;
		}
		return false;
	}






/**
 * Sets a status to 'deleted'
 * @param  char(36) - $id - id of a course
 * @return [type]     [description]
 */
	public function deleteCourseInstance($id) {
		if(empty($id)) {
			return false;
		}
		$this->id = $id;
		$this->saveField('status', 3);
		return true;
	}


/**
 * Gets the course_id of a specified course instance
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
	public function courseId($id) {
		$this->id = $id;
		return $this->field('course_id');
	}





}