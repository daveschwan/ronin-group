<?php
class Order extends AppModel {



/**
 * Gets a specicfic order
 * @param  array  $options [description]
 * @return [type]          [description]
 */
	public function getOrder($options = array()) {

		$defaults = array(
			'id' => null, // method to find order
		);

		extract(array_merge($defaults, $options));

		if(empty($id) && empty($email)) {
			return false;
		}

		$params = array(
			'conditions' => array(),
		);

		if(!empty($id)) {
			$params['conditions'][$this->alias . '.id'] = $id;
		}
		$order = $this->find('first', $params);
		if(empty($order)) {
			return false;
		}
		return $order;

	}




/**
 * Gets an array of orders
 * @return [type] [description]
 */
	public function getOrders($opts = array()) {

		$defaults = array(
			'limit' => 20,
			'findType' => 'all'
		);
		$params = extract(array_merge($defaults, $opts));
		$query = array(
			'conditions' => array(),
			'limit' => $limit,
			'order' => 'Order.created ASC',
		);

		//active only
		if(!empty($activeOnly)) {
			$query['conditions'][$this->alias . '.active'] = 1;
		}

		//limit
		if(!empty($limit)) {
			$query['limit'] = $limit;
		}

		if(!empty($paginate)) {
			return $query;
		} else {
			$orders = $this->find($findType, $query);
			return $orders;
		}
	}



/**
 * [add description]
 * @param [type] $data [description]
 */
	public function add($data) {
		if(empty($data)) {
			return false;
		}

		//creates the new order
		$order = $this->saveAll($data);
		return $order;
	}




/**
 * Edits an existing order info
 * @return [type] [description]
 */
	public function edit($data) {
		if($this->save($data)) {
			return true;
		}
		return false;
	}



/**
 * Sets a status to 'deleted'
 * @param  char(36) - $id - id of a order
 * @return [type]     [description]
 */
	public function deleteOrder($id) {
		if(empty($id)) {
			return false;
		}
		$this->id = $id;
		$this->saveField('status', 3);
		return true;
	}



}