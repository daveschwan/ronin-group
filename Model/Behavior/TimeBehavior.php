<?php
App::uses('CakeTime', 'Utility');
class TimeBehavior extends ModelBehavior {

/**
 * Settings to configure the behavior
 *
 * @var array
 */
	public $settings = array();

/**
 * Default settings
 * label 		- The field used to generate the slug from
 * separator 	- the character used to separate the words in the slug
 * length		- the maximum length of the slug
 *
 * Note that trigger will temporary bypass update and act like update is set to true.
 *
 * @var array
 */
	protected $_defaults = array(
		'timeFields' => array(),
		'defaultTimezone' => 'America/New_York',
		'timezoneModel' => null //must be associated to the main model - leave empty if this model
		// 'separator' => '-',
		// 'update' => true,
		// 'length' => 50,
		// 'removeCommonWords' => true
	);



/**
 * Initiate behaviour
 *
 * @param object $Model
 * @param array $settings
 */
	public function setup(Model $Model, $settings = array()) {
		$this->settings[$Model->alias] = array_merge($this->_defaults, $settings);
	}





/**
 * Before it validates an item, it
 * changes any dates/times to the GMT timezone and
 * converts any dates into a correct YYYY-MM-DD HH:MM:SS format
 * @param  array  $options [description]
 * @return [type]          [description]
 */
public function beforeValidate(Model $Model, $options = array()) {

	//combine start_date and start_time
	if(!empty($Model->data[$Model->alias]['start_date']) && !empty($Model->data[$Model->alias]['start_time'])) {
		$Model->data[$Model->alias]['start'] = strftime("%Y-%m-%d", strtotime($Model->data[$Model->alias]['start_date'])) . ' ' . strftime("%H:%M:%S", strtotime($Model->data[$Model->alias]['start_time']));
		unset($Model->data[$Model->alias]['start_date']);
		unset($Model->data[$Model->alias]['start_time']);
	}

	//combine end_date and end_time
	if(!empty($Model->data[$Model->alias]['end_date']) && !empty($Model->data[$Model->alias]['end_time'])) {
		$Model->data[$Model->alias]['end'] = strftime("%Y-%m-%d", strtotime($Model->data[$Model->alias]['end_date'])) . ' ' . strftime("%H:%M:%S", strtotime($Model->data[$Model->alias]['end_time']));
		unset($Model->data[$Model->alias]['end_date']);
		unset($Model->data[$Model->alias]['end_time']);
	}

	//set default timezone for a new item
	if(empty($Model->data[$Model->alias]['id'])) {
		if (empty($Model->data[$Model->alias]['timezone'])) {
			$Model->data[$Model->alias]['timezone'] = $this->settings[$Model->alias]['defaultTimezone'];
		}
	}

	//convert all dates/times to GMT(server time)
	$this->convertTimeDataToServerTime($Model);

    return true;
}



/**
 * AfterFind that triggers another method that creates the timezone-altered fields with suffix "_tz"
 * @param  Model   $Model   [description]
 * @param  [type]  $results [description]
 * @param  boolean $primary [description]
 * @return [type]           [description]
 */
public function afterFind(Model $Model, $results, $primary = false) {

	//adds timezone-altered fields
	if(!empty($results[0][$Model->alias])) {
		$results[0] = $this->buildTimeFields($Model, $results[0]);
	} else if(!empty($results[$Model->alias])) {
		$results = $this->buildTimeFields($Model, $results);
	}
	return $results;
}




/**
 * Goes through the specified timeFields (model setting) and converts
 * them to UTC, saving them in `[field]_utc` field
 * @param  Model  $Model [description]
 * @return [type]        [description]
 */
	public function convertTimeDataToServerTime(Model $Model) {
		$timezone = $this->getTimezone($Model);
		if(!empty($timezone)) {
			foreach($this->settings[$Model->alias]['timeFields'] as $field) {
				if(!empty($Model->data[$Model->alias][$field]) && $Model->data[$Model->alias][$field] != '0000-00-00 00:00:00') {
					$Model->data[$Model->alias][$field] = strftime('%Y-%m-%d %H:%M:%S', strtotime($Model->data[$Model->alias][$field]));
					$Model->data[$Model->alias][$field . '_utc'] = strftime('%Y-%m-%d %H:%M:%S', strtotime($this->convertToServerTime($Model->data[$Model->alias][$field], $timezone)));
				}
			}
		}
	}



/**
 * Retrieves a timezone either from the provided timezone or
 * by using the provided Model id to retrieve it
 * @return [type] [description]
 */
	public function getTimezone(Model $Model, $data = null) {
		if(empty($data) && !empty($Model->data)) {
			$data = $Model->data;
		}
		$timezoneModel = $this->getTimezoneModel($Model);
		$timezone = $this->settings[$Model->alias]['defaultTimezone'];

		//tries to get the timezone from the provided data (if any)
		if(!empty($data[$timezoneModel]['timezone'])) {
			$timezone = $data[$timezoneModel]['timezone'];

		//otherwise, retrieves the timezone if we have the id
		} else if(!empty($data[$Model->alias]['id'])) {
			$timezone = $this->getModelTimezone($Model, $data[$Model->alias]['id']);
		}

		return $timezone;
	}


/**
 * [getEventTimezone description]
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
	public function getModelTimezone(Model $Model, $id) {
		if(empty($id)) {
			return false;
		}
		$timezoneModel = $this->getTimezoneModel($Model);
		if($Model->alias == $timezoneModel) {
			$Model->id = $id;
			$timezone = $Model->field('timezone');
		} else {
			$Model->id = $id;
			$timezoneModelId = $Model->field(strtolower($timezoneModel).'_id');
			$Model->$timezoneModel->id = $timezoneModelId;
			$timezone = $Model->$timezoneModel->field('timezone');
		}
		return $timezone;
	}

/**
 * Gets the model where we're supposed to get the timezone from
 * defaults to the current model but is overridden by a specified one
 * @param  Model  $Model [description]
 * @return [type]        [description]
 */
	public function getTimezoneModel(Model $Model) {
		$timezoneModel = $Model->alias;
		if(!empty($this->settings[$Model->alias]['timezoneModel'])) {
			$timezoneModel = $this->settings[$Model->alias]['timezoneModel'];
		}
		return $timezoneModel;
	}



/**
 * Converts a provided date/time string to the server (GMT) time based on the provided timezone
 * @param  [type] $dateString    [description]
 * @param  [type] $eventTimezone [description]
 * @return [type]                [description]
 */
	public function convertToServerTime($dateString, $timezone) {
	    return CakeTime::toServer($dateString, $timezone);
	}

/**
 * Converts a provided GMT date/time string to the event's time based on the provided timezone
 * @param  [type] $dateString    [description]
 * @param  [type] $eventTimezone [description]
 * @return [type]                [description]
 */
	public function convertToTimezone($dateString, $timezone) {
	    return CakeTime::format(strtotime($dateString), '%Y-%m-%d %H:%M:%S', false, $timezone);
	}

/**
 * Returns the current time (now) in the timezone provided
 * @param  [string] $eventTimezone  eg: America/Denver
 * @return [string]                [description]
 */
	public function getCurrentTimeInTimezone($timezone) {
		return CakeTime::convert(time(), $timezone);
	}



/**
 * creates the timezone-altered fields with suffix "_tz"
 * @param  Model  $Model [description]
 * @param  [type] $data  [description]
 * @return [type]        [description]
 */
	public function buildTimeFields(Model $Model, $data) {
		$timezone = $this->getTimezone($Model, $data);
		foreach($this->settings[$Model->alias]['timeFields'] as $field) {
			if(array_key_exists($field, $data[$Model->alias])) {
				$data[$Model->alias][$field . '_tz'] = '';
				if(!empty($data[$Model->alias][$field]) && !empty($timezone) && $data[$Model->alias][$field] != '0000-00-00 00:00:00') {
					$data[$Model->alias][$field . '_tz'] = strftime('%Y-%m-%d %H:%M:%S', strtotime($this->convertToTimezone($data[$Model->alias][$field], $timezone)));
				}
			}
		}
		return $data;
	}


}