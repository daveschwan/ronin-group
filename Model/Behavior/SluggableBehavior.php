<?php


App::uses('Multibyte', 'I18n');

/**
 * The idea:
 *
 * set $actsAs = array('Sluggable')
 * and $hasMany = array('Slug')
 *
 */

class SluggableBehavior extends ModelBehavior {

/**
 * Settings to configure the behavior
 *
 * @var array
 */
	public $settings = array();

/**
 * Default settings
 * label 		- The field used to generate the slug from
 * separator 	- the character used to separate the words in the slug
 * length		- the maximum length of the slug
 *
 * Note that trigger will temporary bypass update and act like update is set to true.
 *
 * @var array
 */
	protected $_defaults = array(
		'field' => 'name',
		'separator' => '-',
		'update' => true,
		'length' => 50,
		'removeCommonWords' => true
	);



/**
 * Initiate behaviour
 *
 * @param object $Model
 * @param array $settings
 */
	public function setup(Model $Model, $settings = array()) {
		$this->settings[$Model->alias] = array_merge($this->_defaults, $settings);
	}







	/**
 * beforeValidate callback
 *
 * @param object $Model
 */
	public function beforeValidate(Model $Model, $options = array()) {

		//gets the settings
		$settings = $this->settings[$Model->alias];

		//if there is no data, leave
		if(empty($Model->data[$Model->alias])) return false;

		// if there is a slug in the passed data, use it instead of going off the specified field
		if(!empty($Model->data[$Model->alias]['slug'])) {
			$slug = $this->buildSlug($Model, $settings, $Model->data[$Model->alias]['slug']);
			if(!empty($slug)) $Model->data[$Model->alias]['slug'] = $slug;

		// if no slug was passed, build the slug from the model's chosen field
		} else {
			$slug = $this->buildSlug($Model, $settings);

			//appends the slug to the data, and returns true
			if(!empty($slug)) $Model->data[$Model->alias]['slug'] = $slug;
		}

		return true;
	}


/**
 * [buildSlug description]
 * @param  [type] $settings   [description]
 * @param  [type] $modelData  [description]
 * @return [type]             [description]
 */
	private function buildSlug($Model, $settings, $origSlug = null) {

		//sets default for slug
		$slug = '';

		//if a slug was passed, use it
		if(!empty($origSlug)) {
			$slug = $origSlug;

		//otherwise use it's field
		} else if(!empty($Model->data[$Model->alias][$settings['field']])) {
			$slug = $Model->data[$Model->alias][$settings['field']];
		}

		//cleans up the string and builds the slug
		$slug = $this->multibyteSlug($Model, $slug);

		//forces the slug to be unique
		$slug = $this->makeUniqueSlug($Model, $slug);

		return $slug;
	}






/**
 * Searches if the slug already exists and if yes increments it
 *
 * @param object $Model
 * @param string the raw slug
 * @return string The incremented unique slug
 *
 */
	public function makeUniqueSlug(Model $Model, $slug = '') {

		if(empty($slug)) return '';

		//gets the settings
		$settings = $this->settings[$Model->alias];

		$conditions = array();
		$conditions[$Model->alias . '.slug'] = $slug;
		if (!empty($Model->id)) $conditions[$Model->alias . '.id !='] = $Model->id; //don't want to check if it's the same as itself (because we don't want to increment against itself)

		//check for exact duplicate
		$duplicate = $this->findSlugs($Model, $conditions);

		//it found a duplicate
		if(!empty($duplicate)) {

			//finds all slugs that start w/ the same thing
			$conditions = array();
			$conditions[$Model->alias . '.slug LIKE'] = $slug . '%';
			if(!empty($Model->id)) $conditions[$Model->alias . '.id !='] = $Model->id; //don't want to check if it's the same as itself (because we don't want to increment against itself)

			//queries for simlar(s)
			$similars = $this->findSlugs($Model, $conditions);

			//if it found one
			if (!empty($similars)) {
				$similars = Set::extract($similars, '{n}.'.$Model->alias.'.slug'); //what's this line?
				$startSlug = $slug;
				$index = 2;

				//repeath through, incrementing it's suffix until find one that doesn't already exist
				while ($index > 0) {
					if (!in_array($startSlug . $settings['separator'] . $index, $similars)) {
						$slug = $startSlug . $settings['separator'] . $index;
						$index = -1;
					}
					$index++;
				}
			}
		}

		//returns awesomenated slug
		return $slug;
	}




/**
 * [findSlugs description]
 * @param  array  $conditions [description]
 * @return [type]             [description]
 */
	public function findSlugs(Model $Model, $conditions = array()) {
		$slugs = $Model->find('all', array(
			'recursive' => -1,
			'conditions' => $conditions,
			'fields' => array('slug')
		));
		return $slugs;
	}





/**
 * Generates a slug from a (multibyte) string
 *
 * @param object $Model
 * @param string $string
 * @return string
 */
	public function multibyteSlug(Model $Model, $string = null) {
		$str = mb_strtolower($string);
		$str = preg_replace('/\xE3\x80\x80/', ' ', $str);
		$str = preg_replace('[\'s ]', 's ', $str);
		$str = preg_replace('#[:\#\*"()~$^{}`@+=;,<>!&%\.\]\/\'\\\\|\[]#', "\x20", $str);
		$str = str_replace('?', '', $str);
		$str = str_replace('“', '', $str);
		$str = str_replace('”', '', $str);
		$str = str_replace('’', '', $str);
		$str = str_replace('_', '-', $str);
		$str = trim($str);
		$str = trim($str, '-');
		$str = str_replace('----', '-', $str);
		$str = str_replace('---', '-', $str);
		$str = str_replace('--', '-', $str);

		//remove common words
		$words = explode($this->settings[$Model->alias]['separator'], $str);
		if($this->settings[$Model->alias]['removeCommonWords']) {
			$commonEnglishWords = array('a','able','about','above','abroad','according','accordingly','across','actually','adj','after','afterwards','again','against','ago','ahead','ain\'t','all','allow','allows','almost','alone','along','alongside','already','also','although','always','am','amid','amidst','among','amongst','an','and','another','any','anybody','anyhow','anyone','anything','anyway','anyways','anywhere','apart','appear','appreciate','appropriate','are','aren\'t','around','as','a\'s','aside','ask','asking','associated','at','available','away','awfully','b','back','backward','backwards','be','became','because','become','becomes','becoming','been','before','beforehand','begin','behind','being','believe','below','beside','besides','best','better','between','beyond','both','brief','but','by','c','came','can','cannot','cant','can\'t','caption','cause','causes','certain','certainly','changes','clearly','c\'mon','co','co.','com','come','comes','concerning','consequently','consider','considering','contain','containing','contains','corresponding','could','couldn\'t','course','c\'s','currently','d','dare','daren\'t','definitely','described','despite','did','didn\'t','different','directly','do','does','doesn\'t','doing','done','don\'t','down','downwards','during','e','each','edu','eg','eight','eighty','either','else','elsewhere','end','ending','enough','entirely','especially','et','etc','even','ever','evermore','every','everybody','everyone','everything','everywhere','ex','exactly','example','except','f','fairly','far','farther','few','fewer','fifth','first','five','followed','following','follows','for','forever','former','formerly','forth','forward','found','four','from','further','furthermore','g','get','gets','getting','given','gives','go','goes','going','gone','got','gotten','greetings','h','had','hadn\'t','half','happens','hardly','has','hasn\'t','have','haven\'t','having','he','he\'d','he\'ll','hello','help','hence','her','here','hereafter','hereby','herein','here\'s','hereupon','hers','herself','he\'s','hi','him','himself','his','hither','hopefully','how','howbeit','however','hundred','i','i\'d','ie','if','ignored','i\'ll','i\'m','immediate','in','inasmuch','inc','inc.','indeed','indicate','indicated','indicates','inner','inside','insofar','instead','into','inward','is','isn\'t','it','it\'d','it\'ll','its','it\'s','itself','i\'ve','j','just','k','keep','keeps','kept','know','known','knows','l','last','lately','later','latter','latterly','least','less','lest','let','let\'s','like','liked','likely','likewise','little','look','looking','looks','low','lower','ltd','m','made','mainly','make','makes','many','may','maybe','mayn\'t','me','mean','meantime','meanwhile','merely','might','mightn\'t','mine','minus','miss','more','moreover','most','mostly','mr','mrs','much','must','mustn\'t','my','myself','n','name','namely','nd','near','nearly','necessary','need','needn\'t','needs','neither','never','neverf','neverless','nevertheless','new','next','nine','ninety','no','nobody','non','none','nonetheless','noone','no-one','nor','normally','not','nothing','notwithstanding','novel','now','nowhere','o','obviously','of','off','often','oh','ok','okay','old','on','once','one','ones','one\'s','only','onto','opposite','or','other','others','otherwise','ought','oughtn\'t','our','ours','ourselves','out','outside','over','overall','own','p','particular','particularly','past','per','perhaps','placed','please','plus','possible','presumably','probably','provided','provides','q','que','quite','qv','r','rather','rd','re','really','reasonably','recent','recently','regarding','regardless','regards','relatively','respectively','right','round','s','said','same','saw','say','saying','says','second','secondly','see','seeing','seem','seemed','seeming','seems','seen','self','selves','sensible','sent','serious','seriously','seven','several','shall','shan\'t','she','she\'d','she\'ll','she\'s','should','shouldn\'t','since','six','so','some','somebody','someday','somehow','someone','something','sometime','sometimes','somewhat','somewhere','soon','sorry','specified','specify','specifying','still','sub','such','sup','sure','t','take','taken','taking','tell','tends','th','than','thank','thanks','thanx','that','that\'ll','thats','that\'s','that\'ve','the','their','theirs','them','themselves','then','thence','there','thereafter','thereby','there\'d','therefore','therein','there\'ll','there\'re','theres','there\'s','thereupon','there\'ve','these','they','they\'d','they\'ll','they\'re','they\'ve','thing','things','think','third','thirty','this','thorough','thoroughly','those','though','three','through','throughout','thru','thus','till','to','together','too','took','toward','towards','tried','tries','truly','try','trying','t\'s','twice','two','u','un','under','underneath','undoing','unfortunately','unless','unlike','unlikely','until','unto','up','upon','upwards','us','use','used','useful','uses','using','usually','v','value','various','versus','very','via','viz','vs','w','want','wants','was','wasn\'t','way','we','we\'d','welcome','well','we\'ll','went','were','we\'re','weren\'t','we\'ve','what','whatever','what\'ll','what\'s','what\'ve','when','whence','whenever','where','whereafter','whereas','whereby','wherein','where\'s','whereupon','wherever','whether','which','whichever','while','whilst','whither','who','who\'d','whoever','whole','who\'ll','whom','whomever','who\'s','whose','why','will','willing','wish','with','within','without','wonder','won\'t','would','wouldn\'t','x','y','yes','yet','you','you\'d','you\'ll','your','you\'re','yours','yourself','yourselves','you\'ve','z','zero');
			$str = preg_replace('/\b('.implode('|',$commonEnglishWords).')\b/', '', $str);
		}

		//replaces spaces with separator
		$str = preg_replace('#\x20+#', $this->settings[$Model->alias]['separator'], $str);
		return trim($str);
	}


}
