<?php
class MerchandiseItem extends AppModel {

	public $belongsTo = array(
		'Merchandise'
	);

	public $hasAndBelongsToMany = array(
		'MerchandiseOptionCategory'
	);

}