<?php
class Course extends AppModel {


	public $actsAs = array(
		'Sluggable' => array(
			'field'             => 'name',
			'update'            => 'true',
			'removeCommonWords' => false
		),
	);

	public $hasMany = array(
		'CourseInstance'
	);


/**
 * Data validation
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule'    => array('notEmpty'),
				'message' => 'A course name is required.',
				'last'    => true
			),
			'length' => array(
				'rule'    => array('between', 2, 120),
				'message' => 'Password must be between 2 and 120 characters.',
				'last'    => true
			),
		),
	);











/**
 * Gets a specicfic course
 * @param  array  $options [description]
 * @return [type]          [description]
 */
	public function getCourse($options = array()) {

		$defaults = array(
			'id' => null, // method to find course
			'slug' => null,
			'activeOnly' => true,
		);

		extract(array_merge($defaults, $options));

		if(empty($id) && empty($email)) {
			return false;
		}

		$params = array(
			'conditions' => array(
				'NOT' => array(
					'status' => 3
				)
			),
			'contain' => array(
				'CourseInstance'
			)
		);

		if(!empty($activeOnly)) {
			$params['conditions'][$this->alias . '.active'] = true;
		}

		if(!empty($id)) {
			$params['conditions'][$this->alias . '.id'] = $id;
		}

		if(!empty($slug)) {
			$params['conditions'][$this->alias . '.slug'] = $slug;
		}

		$course = $this->find('first', $params);
		if(empty($course)) {
			return false;
		}
		return $course;

	}




/**
 * Gets an array of courses
 * @return [type] [description]
 */
	public function getCourses($opts = array()) {

		$defaults = array(
			'activeOnly' => true,
			'limit' => 20,
			'findType' => 'all'
		);
		$params = extract(array_merge($defaults, $opts));
		$query = array(
			'conditions' => array(
				'NOT' => array(
					'status' => 3
				)
			),
			'contain' => array(
				'CourseInstance'
			),
			'limit' => $limit,
			'order' => 'Course.name ASC',
		);

		//active only
		if(!empty($activeOnly)) {
			$query['conditions'][$this->alias . '.active'] = 1;
		}

		//limit
		if(!empty($limit)) {
			$query['limit'] = $limit;
		}

		if(!empty($paginate)) {
			return $query;
		} else {
			$courses = $this->find($findType, $query);
			return $courses;
		}
	}



/**
 * Adds a new course
 * @param [type] $data [description]
 */
	public function add($data) {
		if(empty($data)) {
			return false;
		}

		//creates the new course
		$course = $this->saveAll($data);
		return $course;
	}




/**
 * Edits an existing course
 * @return [type] [description]
 */
	public function edit($data) {
		if($this->save($data)) {
			return true;
		}
		return false;
	}





/**
 * Sets a status to 'deleted'
 * @param  char(36) - $id - id of a course
 * @return [type]     [description]
 */
	public function deleteCourse($id) {
		if(empty($id)) {
			return false;
		}
		$this->id = $id;
		$this->saveField('status', 3);
		return true;
	}




}