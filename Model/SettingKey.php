<?php
App::uses('AppModel', 'Model');

class SettingKey extends AppModel {

	public $hasMany = array(
		'Setting',
	);

/**
 * Returns all the partner setting keys
 * @return [type] [description]
 */
	public function getAll() {
		return $this->find('all');
	}

/**
 * Gets the settings for a nested within an array of all setting keys
 * @param  [type] $id [description]
 * @return [type]          [description]
 */
	public function getSettings() {
		$settings = $this->find('all', array(
			'conditions' => array(
				'active' => 1
			),
			'contain' => array(
				'Setting'
			)
		));
		return $settings;
	}

}