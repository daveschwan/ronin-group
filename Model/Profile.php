<?php
class Profile extends AppModel {



/**
 * Belongs To association
 * @var array
 */
	public $belongsTo = array(
		'User',
	);


/**
 * Data validation
 * @var array
 */
	public $validate = array(
		'name' => array(
			'valid' => array(
				'rule'    => array('between', 3, 50),
				'message' => 'Name must be between 3 and 50 characters.',
				'last'    => true
			),
			'notEmpty' => array(
				'rule'    => array('notEmpty'),
				'message' => 'An name is required.',
				'last'    => true
			),
		)
	);


}