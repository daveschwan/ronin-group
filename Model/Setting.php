<?php
App::uses('AppModel', 'Model');

class Setting extends AppModel {

	public $belongsTo = array(
		'SettingKey',
	);

/**
 * Returns all the settings
 * @return [type] [description]
 */
	public function getAll($partnerId) {
		return $this->find('all', array(
			'conditions' => array(
				'partner_id' => $partnerId
			),
			'order' => 'PartnerSetting.order ASC'
		));
	}

/**
 * Updates a group of settings
 * @param  [type] $data [description]
 * @return [type]       [description]
 */
	public function updateSettings($data) {
		if($this->saveAll($data)) {
			return true;
		}
	}

}