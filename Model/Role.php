<?php
App::uses('AppModel', 'Model');

class Role extends AppModel {

	public $hasMany = array(
		'User' => array(
			'dependent' => true,
		)
	);

	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'A name is required.'
			),
			'unique' => array(
				'rule' => 'isUnique',
				'required' => 'create',
				'message' => 'A role with that name already exists.'
			),
			'length' => array(
				'rule' => array('between', 3, 50),
				'message' => 'Name must be between 3 and 50 characters.'
			)
		)
	);


/**
 * retrieves all active roles
 * @return [type] [description]
 */
	public function getRoles($opts = array()) {
		$defaults = array(
			'activeOnly' => true,
			'findType' => 'all',
			'id' => null,
		);
		extract(array_merge($defaults, $opts));
		$query = array(
			'conditions' => array(),
			'limit'=>100,
			'order' => 'Role.name ASC'
		);

		//active only
		if(!empty($activeOnly)) {
			$query['conditions'][$this->alias . '.active'] = 1;
		}

		// a specific role by id
		if(!empty($id)) {
			$query['conditions']['id'] = $id;
			$findType = 'first';
		}

		//limit
		if(!empty($limit)) {
			$query['limit'] = $params['limit'];
		}

		if(!empty($paginate)) {
			return $query;
		} else {
			$articles = $this->find($findType, $query);
			return $articles;
		}
	}



	/**
	 * returns a Role based on an id
	 * @var [type]
	 */
	public function getRole($id = null) {
		if(empty($id)) return false;
		$role = $this->find('first', array(
			'conditions' => array($this->alias . '.id' => $id),
			'contain' => array(
				'UserAction'
			)
		));
		return $role;
	}

}