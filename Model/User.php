<?php
App::uses('Security', 'Utility');
App::uses('AppModel', 'Model');
class User extends AppModel {

/**
 * Belongs To association
 * @var array
 */
	public $belongsTo = array(
		'Role' => array(
			'counterCache' => true,
		),
	);


/**
 * Has Many association
 * @var array
 */
	public $hasMany = array(
		'Attendee' => array(
			'dependent' => true,
		),
		'Order',
	);


/**
 * Has One association
 * @var array
 */
	public $hasOne = array(
		'Profile' => array(
			'dependent' => true
		),
	);


/**
 * Data validation
 * @var array
 */
	public $validate = array(
		'email' => array(
			'valid' => array(
				'rule'    => array('email'),
				'message' => 'Please supply a valid email address.',
				'last'    => true
			),
			'notEmpty' => array(
				'rule'    => array('notEmpty'),
				'message' => 'An email is required.',
				'last'    => true
			),
			'unique' => array(
				'rule'    => 'isUnique',
				'message' => 'A user with that email address already exists.',
			)
		),
		'password' => array(
			'notEmpty' => array(
				'rule'     => 'notEmpty',
				'on'       => 'create',
				'required' => true,
				'message'  => 'Must include a password',
				'last'     => true
			),
			'length' => array(
				'rule'    => array('between', 8, 20),
				'message' => 'Password must be between 8 and 20 characters.',
				'last'    => true
			),
			'unique' => array(
				'rule'     => array('notEmpty'),
				'required' => true,
				'on'       => 'create',
				'message'  => 'Cannot create a user without a password.'
			),
		),
		're_password' => array(
			'notEmpty' => array(
				'rule'     => 'notEmpty',
				'required' => true,
				'on' => 'create',
				'message' => 'Must confirm password.',
				'last' => true
			),
			'match' => array(
				'rule' => array('equalToField', 'password'),
				'message' => 'Confirmation password must match the password.',
				'last' => true
			)
		),
		'role_id' => array(
			'notEmpty' => array(
				'rule'    => array('notEmpty'),
				'message' => 'A role is required.'
			)
		)
	);







/**
 * Gets a specicfic user
 * @param  array  $options [description]
 * @return [type]          [description]
 */
	public function getUser($options = array()) {

		$defaults = array(
			'id' => null, // method to find user
			'email' => null, // method to find user
			'activeOnly' => true,
		);

		extract(array_merge($defaults, $options));

		if(empty($id) && empty($email)) {
			return false;
		}

		$params = array(
			'conditions' => array(),
			'contain' => array(
				'Role',
				'Profile'
			)
		);

		if(!empty($activeOnly)) {
			$params['conditions'][$this->alias . '.active'] = true;
		}

		if(!empty($id)) {
			$params['conditions'][$this->alias . '.id'] = $id;
		}

		if(!empty($email)) {
			$params['conditions'][$this->alias . '.email'] = $email;
		}

		$user = $this->find('first', $params);
		if(empty($user)) {
			return false;
		}
		return $user;

	}



/**
 * Gets an array of users
 * @return [type] [description]
 */
	public function getUsers($opts = array()) {

		$defaults = array(
			'activeOnly' => true,
			'limit' => 20,
			'findType' => 'all'
		);
		$params = extract(array_merge($defaults, $opts));
		$query = array(
			'conditions' => array(),
			'contain' => array(
				'Role',
				'Profile'
			),
			'limit' => $limit,
			'order' => 'User.name ASC',
		);

		//active only
		if(!empty($activeOnly)) {
			$query['conditions'][$this->alias . '.active'] = 1;
		}

		//limit
		if(!empty($limit)) {
			$query['limit'] = $limit;
		}

		if(!empty($paginate)) {
			return $query;
		} else {
			$users = $this->find($findType, $query);
			return $users;
		}
	}




/**
 * Register a new user
 * @param [type] $data [description]
 */
	public function register($data) {
		if(empty($data)) {
			return false;
		}

		// sets role
		$data['User']['role_id'] = 'F421E0BA-8D59-4E39-9494-1BBD735F2667'; // general user

		// creates the new user
		$user = $this->save($data);
		return $user;
	}



/**
 * [add description]
 * @param [type] $data [description]
 */
	public function add($data) {
		if(empty($data)) {
			return false;
		}

		//creates the new user
		$save = $this->saveAll($data);
		if(!$save) {
			return false;
		}
		return $this->findById($this->id);
	}




/**
 * Edits an existing user's account info (not their profile-info)
 * @return [type] [description]
 */
	public function edit($data) {
		if($this->save($data)) {
			return true;
		}
		return false;
	}



/**
 * Delete an existing user
 * @param  char(36) - $id - id of a user
 * @return [type]     [description]
 */
	public function deleteUser($id) {
		$this->delete($id);
		return true;
	}



/**
 * [beforeSave description]
 * @param  array  $options [description]
 * @return [type]          [description]
 */
	public function beforeSave($options = array()) {

		// Set defaults
		if(!isset($this->data['User']['id'])) {
			$this->data['User']['active'] = 1;
			$this->data['Role']['id'] = '18DD8261-AF42-4D16-B54C-E43808BB42F7'; // standard 'User'
		}

		// Use bcrypt
		if(isset($this->data['User']['password'])) {
			$hash = Security::hash($this->data['User']['password'], 'blowfish');
			$this->data['User']['password'] = $hash;
		}
		return true;
	}




/**
 * compares two fields to make sure they're the same
 * (binary-safe comparisons)
 * @param  [type] $array [description]
 * @param  [type] $field [description]
 * @return [type]        [description]
 */
	function equalToField($array, $field) {
		return strcmp($this->data[$this->alias][key($array)], $this->data[$this->alias][$field]) == 0;
	}




}