<?php

/**
 * An Attendee is a reference to a user that's connected to a specific event instance
 */
class Attendee extends AppModel {

	public $belongsTo = array(
		'CourseInstance',
		'User',
		'Order',
	);





/**
 * Data validation
 * @var array
 */
	public $validate = array(
		'course_instance_id' => array(
			'notEmpty' => array(
				'rule'    => array('notEmpty'),
				'message' => 'Course id is required.',
				'last'    => true
			),
		),
		'user_id' => array(
			'notEmpty' => array(
				'rule'    => array('notEmpty'),
				'message' => 'A user is required.',
				'last'    => true
			),
		),
	);



/**
 * returns the course instance id of a specified attendee
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
	public function courseInstanceId($id) {
		if(empty($id)) {
			return false;
		}
		$this->id = $id;
		return $this->field('course_instance_id');
	}




/**
 * Adds a new attendee
 * @param [type] $data [description]
 */
	public function add($data) {
		if(empty($data)) {
			return false;
		}
		//creates the new attendee
		$attendee = $this->saveAll($data);
		return $attendee;
	}





/**
 * Finds all attendees of a specific course and their User's info
 * @param  [type] $courseInstanceId [description]
 * @return [type]                   [description]
 */
	public function getAttendees($courseInstanceId, $opts = array()) {

		if(empty($courseInstanceId)) {
			return false;
		}

		$defaults = array(
			'limit' => 20,
			'findType' => 'all'
		);
		extract(array_merge($defaults, $opts));

		$query = array(
			'conditions' => array(
				'course_instance_id' => $courseInstanceId
			),
			'contain' => array(
				'User'
			)
		);

		//limit
		if(!empty($limit)) {
			$query['limit'] = $limit;
		}

		if(!empty($paginate)) {
			return $query;
		} else {
			$courseInstances = $this->find($findType, $query);
			return $courseInstances;
		}
		return $this->find('all', array(

		));
	}




/**
 * Deletes an Attendee row (this does NOT delete the user)
 * @param  char(36) - $id - id of a course
 * @return [type]     [description]
 */
	public function deleteAttendee($id) {
		if(empty($id)) {
			return false;
		}
		return $this->delete($id);
	}

}