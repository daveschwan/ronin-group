<?php
App::uses('AppModel', 'Model');
class MerchandiseOptionCategory extends AppModel {


	public $hasMany = array(
		'MerchandiseOption'
	);




/**
 * [getMerchandiseOptionCategories description]
 * @return [type] [description]
 */
	public function getMerchandiseOptionCategories($merchandiseId, $opts = array()) {
		$defaults = array(
			'limit' => 500,
			'findType' => 'all',
		);
		$params = extract(array_merge($defaults, $opts));
		$query = array(
			'conditions' => array(
				'merchandise_id' => $merchandiseId
			),
			'contain' => array(),
			'limit' => $limit,
			'order' => 'MerchandiseOptionCategory.name ASC',
		);

		//limit
		if(!empty($limit)) {
			$query['limit'] = $limit;
		}

		if(!empty($paginate)) {
			return $query;
		} else {
			$users = $this->find($findType, $query);
			return $users;
		}
	}



/**
 * Adds a new Merchandise Option Category
 * @param [type] $data [description]
 */
	public function add($data) {
		if(empty($data)) {
			return false;
		}

		//creates the new article
		$merchandiseOptionCategory = $this->saveAll($data);
		return $merchandiseOptionCategory;
	}


}