<?php
App::uses('AppModel', 'Model');
class Merchandise extends AppModel {




	public $hasMany = array(
		'MerchandiseOptionCategory' => array(
			'counterCache' => true
		),
		'MerchandiseItem'
	);

	public $hasAndBelongsToMany = array(
		'MerchandiseCategory'
	);





/**
 * Gets a specicfic merchandise
 * @param  array  $options [description]
 * @return [type]          [description]
 */
	public function getMerchandise($options = array()) {

		$defaults = array(
			'id' => null, // method to find merchandise
			'slug' => null,
			'activeOnly' => true,
		);

		extract(array_merge($defaults, $options));

		if(empty($id) && empty($email)) {
			return false;
		}

		$params = array(
			'conditions' => array(
				'NOT' => array(
					'status' => 3
				)
			),
			'contain' => array(
				'MerchandiseCategory',
				'MerchandiseItem',
				'MerchandiseOptionCategory' => array(
					'MerchandiseOption'
				)
			)
		);

		if(!empty($activeOnly)) {
			$params['conditions'][$this->alias . '.active'] = true;
		}

		if(!empty($id)) {
			$params['conditions'][$this->alias . '.id'] = $id;
		}

		if(!empty($slug)) {
			$params['conditions'][$this->alias . '.slug'] = $slug;
		}

		$merchandise = $this->find('first', $params);
		if(empty($merchandise)) {
			return false;
		}
		return $merchandise;

	}



/**
 * Gets an array of merchandises
 * @return [type] [description]
 */
	public function getMerchandises($opts = array()) {

		$defaults = array(
			'activeOnly' => true,
			'limit' => 20,
			'findType' => 'all'
		);
		$params = extract(array_merge($defaults, $opts));
		$query = array(
			'conditions' => array(),
			'contain' => array(),
			'limit' => $limit,
			'order' => 'Merchandise.name ASC',
		);

		//active only
		if(!empty($activeOnly)) {
			$query['conditions'][$this->alias . '.active'] = 1;
		}

		//limit
		if(!empty($limit)) {
			$query['limit'] = $limit;
		}

		if(!empty($paginate)) {
			return $query;
		} else {
			$users = $this->find($findType, $query);
			return $users;
		}
	}



/**
 * Adds a new Merchandise item
 * @param [type] $data [description]
 */
	public function add($data) {
		if(empty($data)) {
			return false;
		}

		//creates the new user
		$merchandise = $this->saveAll($data);
		return $merchandise;
	}




/**
 * Edits an existing merchandise
 * @return [type] [description]
 */
	public function edit($data) {
		if($this->save($data)) {
			return true;
		}
		return false;
	}



/**
 * Delete a merchandise
 * @param  char(36) - $id - id of a merchandise
 * @return [type]     [description]
 */
	public function deleteMerchandise($id) {
		$this->delete($id);
		return true;
	}

}