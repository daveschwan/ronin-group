<?php

Router::connect('/register', array('controller'=>'users', 'action'=>'register', 'admin'=>false));
Router::connect('/login', array('controller'=>'users', 'action'=>'login', 'admin'=>false));
Router::connect('/logout', array('controller'=>'users', 'action'=>'logout', 'admin'=>false));
Router::connect('/admin', array('controller'=>'users', 'action'=>'login', 'admin'=>true));

Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));




CakePlugin::routes();
require CAKE . 'Config' . DS . 'routes.php';
