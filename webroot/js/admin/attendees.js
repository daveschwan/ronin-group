function initializePage() {
	var createNewUser = $('#AttendeeCreateUser').val();
	if(createNewUser) {
		showNewUserFields();
	} else {
		showExistingUserFields();
	}
}

function showNewUserFields() {
	$('#existingUserFields').slideUp();
	$('#newUserFields').slideDown();
	$('#existingUserFields select').prop("disabled", true);
	$('#newUserFields input').prop("disabled", false);
	$('#AttendeeCreateUser').val(true);
}

function showExistingUserFields() {
	$('#existingUserFields').slideDown();
	$('#newUserFields').slideUp();
	$('#newUserFields input').prop("disabled", true);
	$('#existingUserFields select').prop("disabled", false);
	$('#AttendeeCreateUser').val(false);
}