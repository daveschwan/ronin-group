$(document).ready(function() {
	$('#timezones').change(function() {
		updateTimeZone($(this).val());
	});
});

function updateTimeZone(timezone) {
	$('#timezones').val(timezone);
	var myArr = timezone.split("/");
	var location = myArr[1];
	// console.log(location);

	$('[id^="timezonePreset_"]').removeClass('at');
	$('#timezonePreset_'+location).addClass('at');
}