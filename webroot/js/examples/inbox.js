$(document).ready(function() {

	// PAGE RELATED SCRIPTS

	/*
	 * Fixed table height
	 */

	tableHeightSize()

	$(window).resize(function() {
		tableHeightSize()
	})

	function tableHeightSize() {
		var tableHeight = $(window).height() - 212;
		$('.table-wrap').css('height', tableHeight + 'px');
	}

	/*
	 * LOAD INBOX MESSAGES
	 */
	loadInbox();
	function loadInbox() {
		loadURL("/ajax/email/email-list.php", $('#inbox-content > .table-wrap'))
	}

	/*
	 * Buttons (compose mail and inbox load)
	 */
	$(".inbox-load").click(function() {
		loadInbox();
	});

	// compose email
	$("#compose-mail").click(function() {
		loadURL("/ajax/email-compose.php", $('#inbox-content > .table-wrap'));
	})

});