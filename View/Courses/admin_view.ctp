<?php
echo $this->Html->script(
	array(
		'admin/courses'
	),
	array(
		'inline'=>false
	)
);
?>

<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-calendar fa-fw "></i>
			<?php echo $this->Html->link('Courses', array('controller'=>'courses', 'action'=>'index')); ?>
			<span>&nbsp;&nbsp; &gt; &nbsp;&nbsp;</span>
			View Course
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2><?php echo $course['Course']['name']; ?></h2>
			</header>

			<div role="content">

				<div class="widget-body">
					<?php
					echo $course['Course']['description'];
					echo '<br><br>';
					echo $this->Html->link('Edit this Course', array('controller'=>'courses', 'action'=>'edit', $course['Course']['id']));
					?>
				</div>

			</div>
		</div>

	</article>
</div>




<div class="row">
	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading" class="drk">
				<span class="widget-icon"> <i class="fa fa-map-marker"></i> </span>
				<h2>Course Instances</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th><?php echo $this->Paginator->sort('CourseInstance.location', 'Location'); ?></th>
								<th><?php echo $this->Paginator->sort('CourseInstance.start_utc', 'Start'); ?></th>
								<th class="center"><?php echo $this->Paginator->sort('CourseInstance.attendee_count', 'Attendees'); ?></th>
								<th class="center"><?php echo $this->Paginator->sort('CourseInstance.active', 'Active'); ?></th>
								<th class="center">Delete</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($courseInstances as $courseInstance) {
								?>
								<tr>
									<td><?php echo $this->Html->link($courseInstance['CourseInstance']['location'], array('controller'=>'course_instances', 'action'=>'view', $courseInstance['CourseInstance']['id'])); ?></td>
									<td><?php echo $this->Time->nice($courseInstance['CourseInstance']['start']); ?></td>
									<td class="center"><?php echo $courseInstance['CourseInstance']['attendee_count']; ?></td>
									<td class="center"><?php echo ($courseInstance['CourseInstance']['active']) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>'; ?></td>
									<td class="center">
										<?php
										echo $this->Html->link('<i class="fa fa-times fa-lg"></i>', array('controller'=>'course_instances', 'action'=>'delete', $courseInstance['CourseInstance']['id']), array('escape'=>false), 'Are you sure you want to delete the date: "' . $this->Time->nice($courseInstance['CourseInstance']['start']) . '"?');
										?>
									</td>
								</tr>
							<?php
							}
							?>
						</tbody>
					</table>
					<?php
					echo $this->Html->link('Add an Instance', array('controller'=>'course_instances', 'action'=>'add', $course['Course']['id']), array('class'=>'btn-bottom-add btn btn-primary'));
					echo $this->Element('admin/paging');
					?>

				</div>


			</div>

		</div>

	</article>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
		<?php echo $this->Html->link('Delete this Course', array('controller'=>'courses', 'action'=>'delete', $course['Course']['id']), array(), "Are you sure you want to delete this course?"); ?>
	</article>

</div>