
<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-calendar fa-fw "></i>
			Courses
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading" class="drk">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2>Course List</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th><?php echo $this->Paginator->sort('Course.name', 'Name'); ?></th>
								<th>Description</th>
								<th class="center"><?php echo $this->Paginator->sort('Course.course_instance_count', '# Dates'); ?></th>
								<th class="center"><?php echo $this->Paginator->sort('Course.active', 'Active'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($courses as $course) {
								?>
								<tr>
									<td><?php echo $this->Html->link($course['Course']['name'], array('controller'=>'courses', 'action'=>'view', $course['Course']['id'])); ?></td>
									<td><?php echo $this->Text->truncate(strip_tags($course['Course']['description']), 60); ?></td>
									<td class="center"><?php echo $course['Course']['course_instance_count']; ?></td>
									<td class="center">
										<?php
										$icon = ($course['Course']['active']) ? 'fa-check' : 'fa-minus faded';
										echo '<i class="fa ' . $icon . ' fa-lg"></i>'
										?>
									</td>
								</tr>
							<?php
							}
							?>
						</tbody>
					</table>
					<?php
					echo $this->Html->link('Add a Course', array('controller'=>'courses', 'action'=>'add'), array('class'=>'btn-bottom-add btn btn-primary'));
					echo $this->Element('admin/paging');
					?>

				</div>

			</div>

		</div>

	</article>
</div>