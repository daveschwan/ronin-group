<?php
echo $this->Html->script(
	array(
		'admin/courses'
	),
	array(
		'inline'=>false
	)
);
?>

<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-calendar fa-fw "></i>
			<?php echo $this->Html->link('Courses', array('controller'=>'courses', 'action'=>'index')); ?>
			<span>&nbsp;&nbsp; &gt; &nbsp;&nbsp;</span>
			<?php echo $course['Course']['name']; ?>
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2>General Course Info</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<?php
					echo $this->Form->create('Course', array('inputDefaults'=>array('label'=>false,'div'=>false)));
						echo $this->Form->input('Course.id');
						?>
						<fieldset>

							<div class="form-group">
								<label>Name</label>
								<?php echo $this->Form->input('Course.name', array('class'=>'form-control input-lg')); ?>
								<p class="note">The full name of the course.  Example: Advanced Rifle Training"</p>
							</div>

							<div class="form-group">
								<label>Description</label>
								<?php echo $this->Form->input('Course.description', array('class'=>'rte form-control input-lg tall')); ?>
								<p class="note">General description of the course.</p>
							</div>

							<div class="form-group">
								<div class="checkbox">
									<label>
										<?php echo $this->Form->input('Course.active', array('class'=>'checkbox style-0')); ?>
										<span>Active (Visible)</span>
									</label>
								</div>
								<p class="note">Whether or not this course is visible to users.</p>
							</div>

						</fieldset>


						<fieldset>

							<div id="advanced-area" style="display:none;">
								<div class="form-group">
									<label>Slug (URL-name)</label>
									<?php echo $this->Form->input('Course.slug', array('class'=>'form-control')); ?>
									<p class="note">ADVANCED USE ONLY: The name (no spaces or special characters) of the course used in the web address for this course.<br>
									WARNING:  If you have given out the link to this course, changing this will break those links.</p>
								</div>
							</div>

							<div id="show-advanced-button-area">
								<a href="javascript:showAdvancedSettings();">Show Advanced Settings</a>
							</div>

							<div id="hide-advanced-button-area" style="display:none;">
								<a href="javascript:hideAdvancedSettings();">Hide Advanced Settings</a>
							</div>

						</fieldset>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->Html->link('Cancel', array('controller'=>'courses', 'action'=>'index'), array('class'=>'btn btn-default')); ?>
									<?php echo $this->Form->submit('Save Changes to Course', array('class'=>'btn btn-primary', 'div'=>false)); ?>
								</div>
							</div>
						</div>

					<?php
					echo $this->Form->end();
					?>

				</div>

			</div>

		</div>

	</article>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12">
		<?php echo $this->Html->link('Delete this Course', array('controller'=>'courses', 'action'=>'delete', $course['Course']['id']), array(), "Are you sure you want to delete this course?"); ?>
	</article>

</div>





<script>
	function showAdvancedSettings() {
		$('#advanced-area').show();
		$('#advanced-area input').removeAttr('disabled');
		$('#hide-advanced-button-area').show();
		$('#show-advanced-button-area').hide();
		$('#UserPassword').focus();
	}
	function hideAdvancedSettings() {
		$('#advanced-area').hide();
		$('#advanced-area input').prop('disabled', 'disabled');
		$('#hide-advanced-button-area').hide();
		$('#show-advanced-button-area').show();
	}
	function addCourseInstance() {

	}
</script>