
<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-calendar fa-fw "></i>
			<?php echo $this->Html->link('Courses', array('controller'=>'courses', 'action'=>'index')); ?>
			<span>&nbsp;&nbsp; &gt; &nbsp;&nbsp;</span>
			Add
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2>General Course Info</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<?php
					echo $this->Form->create('Course', array('inputDefaults'=>array('label'=>false,'div'=>false)));
						?>
						<fieldset>

							<div class="form-group">
								<label>Name</label>
								<?php echo $this->Form->input('Course.name', array('class'=>'form-control input-lg')); ?>
								<p class="note">The full name of the course.  Example: Advanced Rifle Training"</p>
							</div>

							<div class="form-group">
								<label>Description</label>
								<?php echo $this->Form->input('Course.description', array('class'=>'rte form-control input-lg tall')); ?>
								<p class="note">General description of the course.</p>
							</div>

						</fieldset>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->Html->link('Cancel', array('controller'=>'courses', 'action'=>'index'), array('class'=>'btn btn-default')); ?>
									<?php echo $this->Form->submit('Add Course', array('class'=>'btn btn-primary', 'div'=>false)); ?>
								</div>
							</div>
						</div>

					<?php
					echo $this->Form->end();
					?>

				</div>

			</div>

		</div>

	</article>
</div>