<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-calendar fa-fw "></i>
			<?php echo $this->Html->link('Course Instances', array('controller'=>'course_instances', 'action'=>'index')); ?>
			<span>&nbsp;&nbsp; &gt; &nbsp;&nbsp;</span>
			View Course Instance
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading">
				<span class="widget-icon"> <i class="fa fa-info"></i> </span>
				<h2>Details</h2>
			</header>

			<div role="content">

				<div class="widget-body">
					<?php
					echo '<strong>Name:</strong><br>';
					echo $courseInstance['CourseInstance']['location'] . '<br><br>';

					echo '<strong>Details:</strong><br>';
					echo $courseInstance['CourseInstance']['details'] . '<br><br>';
					echo $this->Html->link('Edit this Course Instance', array('controller'=>'course_instances', 'action'=>'edit', $courseInstance['CourseInstance']['id']));
					?>
				</div>

			</div>
		</div>

	</article>
</div>




<div class="row">
	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading" class="drk">
				<span class="widget-icon"> <i class="fa fa-user"></i> </span>
				<h2>Attendees</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th><?php echo $this->Paginator->sort('Attendee.name', 'Name'); ?></th>
								<th>Email</th>
								<th class="center"><?php echo $this->Paginator->sort('Attendee.paid', 'Paid'); ?></th>
								<th class="center"><?php echo $this->Paginator->sort('Attendee.paid_date', 'Paid Date'); ?></th>
								<th class="center">Remove</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($attendees as $attendee) {
								?>
								<tr>
									<td><?php echo $this->Html->link($attendee['Attendee']['name'], array('controller'=>'attendees', 'action'=>'edit', $attendee['Attendee']['id'])); ?></td>
									<td><?php echo $attendee['User']['email']; ?></td>
									<td class="center"><?php echo $this->Time->nice($attendee['Attendee']['paid_date']); ?></td>
									<td class="center"><?php echo ($attendee['Attendee']['paid']) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-minus"></i>'; ?></td>
									<td class="center">
										<?php
										echo $this->Html->link('<i class="fa fa-times fa-lg"></i>', array('controller'=>'attendees', 'action'=>'delete', $attendee['Attendee']['id']), array('escape'=>false), 'Are you sure you want to remove this attendee: "' . $attendee['Attendee']['name'] . '"?');
										?>
									</td>
								</tr>
							<?php
							}
							?>
						</tbody>
					</table>
					<?php
					echo $this->Html->link('Add an Attendee', array('controller'=>'attendees', 'action'=>'add', $courseInstance['CourseInstance']['id']), array('class'=>'btn-bottom-add btn btn-primary'));
					echo $this->Element('admin/paging');
					?>

				</div>


			</div>

		</div>

	</article>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
		<?php echo $this->Html->link('Delete this Course', array('controller'=>'courses', 'action'=>'delete', $course['Course']['id']), array(), "Are you sure you want to delete this course?"); ?>
	</article>

</div>