<?php
$this->Html->css(
	array(
		'/bootstrap-datepicker/css/datepicker',
		'/bootstrap-timepicker/css/bootstrap-timepicker.min.css'
	),
	null,
	array(
		'inline'=>false
	)
);
$this->Html->script(
	array(
		'/bootstrap-datepicker/js/bootstrap-datepicker',
		'/bootstrap-timepicker/js/bootstrap-timepicker.min',
		'moment-with-langs.min',
	),
	array('inline'=>false, 'block'=>'scriptTop')
);
?>

<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-calendar fa-fw "></i>
			<?php echo $this->Html->link($this->Text->truncate($course['Course']['name'], 20), array('controller'=>'courses', 'action'=>'view', $course['Course']['id'])); ?>
			<span>&nbsp;&nbsp; &gt; &nbsp;&nbsp;</span>
			<?php echo $this->Html->link($this->Text->truncate($courseInstance['CourseInstance']['location'], 20), array('controller'=>'course_instances', 'action'=>'view', $courseInstance['CourseInstance']['id'])); ?>
			<span>&nbsp;&nbsp; &gt; &nbsp;&nbsp;</span>
			Edit Instance
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2>Course Instance Info</h2>
			</header>

			<div role="content">

				<div class="widget-body">
					<div class="row">

						<?php
						echo $this->Form->create('CourseInstance', array('inputDefaults'=>array('label'=>false,'div'=>false)));
							echo $this->Form->input('id');
							?>
							<fieldset>

								<div class="form-group col-md-12">
									<label>Location</label>
									<?php echo $this->Form->input('location', array('class'=>'form-control input-lg')); ?>
									<p class="note">The general name of the location.  Eg. "North Point Range"</p>
								</div>

								<div class="clearfix form-group col-md-12">
									<div class="row">
			 							<div class="pull-left col-sm-3">
											<label id="label_start">Start Date</label>
											<?php
											$startValue = '';
											if(!empty($courseInstance['CourseInstance']['start'])) {
												$startValue = $this->Time->format($courseInstance['CourseInstance']['start'], '%m/%d/%Y');
											}
											echo $this->Form->input('start_date', array('class'=>'form-control date-field', 'type'=>'text', 'value'=>$startValue)); //'%a. %B %e, %Y'
											?>
										</div>
										<div class="pull-left col-sm-3">
											<label>From</label>
									        <div class="input-group bootstrap-timepicker">
									             <?php echo $this->Form->input('start_time', array('class'=>'form-control input-small')); //'%a. %B %e, %Y' ?>
							                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
									        </div>
										</div>
										<div class="pull-left col-sm-3">
											<label>Until</label>
									        <div class="input-group bootstrap-timepicker">
									             <?php echo $this->Form->input('end_time', array('class'=>'form-control input-small')); //'%a. %B %e, %Y' ?>
							                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
									        </div>
										</div>
				 						<div class="pull-left col-sm-3">
											<label id="label_end">End Date</label>
											<?php
											$endValue = '';
											if(!empty($courseInstance['CourseInstance']['end'])) {
												$endValue = $this->Time->format($courseInstance['CourseInstance']['end'], '%m/%d/%Y');
											}
											echo $this->Form->input('end_date', array('class'=>'form-control date-field', 'date-format'=>'MM/dd/yyyy', 'type'=>'text', 'value'=>$endValue));
											?>
										</div>
									</div>
								</div>

								<div class="form-group col-md-12">
									<label>Active</label><br>
									<?php echo $this->Form->input('active', array()); ?>
									<p class="note">Whether or not this course instance is visible to users.</p>
								</div>

								<div class="form-group col-md-12">
									<label>Details</label>
									<?php echo $this->Form->input('details', array('class'=>'form-control input-lg')); ?>
									<p class="note">Details about this specific instance. (Course details will already be shown)</p>
								</div>

								<div class="form-group col-md-12">
									<label>Address</label>
									<?php echo $this->Form->input('address1', array('class'=>'form-control input-lg')); ?>
								</div>

								<div class="form-group col-md-12">
									<label>Address (line 2)</label>
									<?php echo $this->Form->input('address2', array('class'=>'form-control input-lg')); ?>
								</div>

								<div class="form-group col-md-6">
									<label>City</label>
									<?php echo $this->Form->input('city', array('class'=>'form-control input-lg')); ?>
								</div>

								<div class="form-group col-md-6">
									<label>US State</label>
									<?php echo $this->Form->input('us_state', array('class'=>'form-control input-lg')); ?>
								</div>

								<div class="form-group col-md-12">
									<label>Timezone</label><br>
									<?php echo $this->TimeZone->select(array('class'=>'norm form-control input-lg', 'selected' => Configure::read('Timezone.default'))); ?>
								</div>

							</fieldset>

							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<?php echo $this->Html->link('Cancel', array('controller'=>'courses', 'action'=>'view', $course['Course']['id']), array('class'=>'btn btn-default')); ?>
										<?php echo $this->Form->submit('Save', array('class'=>'btn btn-primary', 'div'=>false)); ?>
									</div>
								</div>
							</div>

						<?php
						echo $this->Form->end();
						?>
					</div>

				</div>

			</div>

		</div>

	</article>
</div>

<script>

$(document).ready(function() {
	$('#CourseInstanceStartTime').timepicker();
	$('#CourseInstanceEndTime').timepicker();

	// http://www.eyecon.ro/bootstrap-datepicker/
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

	var startDatePicker = $('#CourseInstanceStartDate').datepicker({
		format: 'mm/dd/yyyy',
	}).on('changeDate', function(ev) {
		$('#CourseInstanceStartDate').datepicker('hide');
		if (ev.date.valueOf() > endDatePicker.date.valueOf()) {
			var newDate = new Date(ev.date);
			// newDate.setDate(newDate.getDate() + 1);
			endDatePicker.setValue(newDate);
		}
	}).data('datepicker');

	var endDatePicker = $('#CourseInstanceEndDate').datepicker({
		onRender: function(date) {
			return date.valueOf() < startDatePicker.date.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev) {
		$('#CourseInstanceEndDate').datepicker('hide');
	}).data('datepicker');
});

</script>