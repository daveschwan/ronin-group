<div class="modal fade" id="addDateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					×
				</button>
				<h4 class="modal-title" id="myModalLabel">Course Instance</h4>
			</div>
			<div class="modal-body">

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<?php //echo $this->Form->input('CourseInstance.location'); ?>
							<input type="text" class="form-control" placeholder="Title" required="">
						</div>
						<div class="form-group">
							<textarea class="form-control" placeholder="Content" rows="5" required=""></textarea>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="category"> Category</label>
							<select class="form-control" id="category">
								<option>Articles</option>
								<option>Tutorials</option>
								<option>Freebies</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="tags"> Tags</label>
							<input type="text" class="form-control" id="tags" placeholder="Tags">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="well well-sm well-primary">
							<form class="form form-inline " role="form">
								<div class="form-group">
									<input type="text" class="form-control" value="" placeholder="Date" required="">
								</div>
								<div class="form-group">
									<select class="form-control">
										<option>Draft</option>
										<option>Published</option>
									</select>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-success btn-sm">
										<span class="glyphicon glyphicon-floppy-disk"></span> Save
									</button>
									<button type="button" class="btn btn-default btn-sm">
										<span class="glyphicon glyphicon-eye-open"></span> Preview
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					Cancel
				</button>
				<button type="button" class="btn btn-primary">
					Post Article
				</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>