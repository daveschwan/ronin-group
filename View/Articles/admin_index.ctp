
<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-calendar fa-fw "></i>
			Articles
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading" class="drk">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2>Articles List</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th><?php echo $this->Paginator->sort('Article.title', 'Name'); ?></th>
								<th>Subtitle</th>
								<th class="center"><?php echo $this->Paginator->sort('Article.created', 'Created'); ?></th>
								<th class="center"><?php echo $this->Paginator->sort('Article.active', 'Active'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($articles as $article) {
								?>
								<tr>
									<td><?php echo $this->Html->link($this->Text->truncate($article['Article']['title'], 60), array('controller'=>'articles', 'action'=>'edit', $article['Article']['id'])); ?></td>
									<td><?php echo $this->Text->truncate(strip_tags($article['Article']['content']), 60); ?></td>
									<td><?php echo $this->Time->niceShort($article['Article']['created']); ?></td>
									<td class="center">
										<?php
										$icon = ($article['Article']['active']) ? 'fa-check' : 'fa-minus faded';
										echo '<i class="fa ' . $icon . ' fa-lg"></i>'
										?>
									</td>
								</tr>
							<?php
							}
							?>
						</tbody>
					</table>
					<?php
					echo $this->Html->link('Add an Article', array('controller'=>'articles', 'action'=>'add'), array('class'=>'btn-bottom-add btn btn-primary'));
					echo $this->Element('admin/paging');
					?>

				</div>

			</div>

		</div>

	</article>
</div>