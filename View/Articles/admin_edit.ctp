
<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-calendar fa-fw "></i>
			<?php echo $this->Html->link('Articles', array('controller'=>'articles', 'action'=>'index')); ?>
			<span>&nbsp;&nbsp; &gt; &nbsp;&nbsp;</span>
			Edit
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2>General Article Info</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<?php
					echo $this->Form->create('Article', array('inputDefaults'=>array('label'=>false,'div'=>false)));
						echo $this->Form->input('Article.id');
						?>
						<fieldset>

							<div class="form-group">
								<label>Title</label>
								<?php echo $this->Form->input('Article.title', array('class'=>'form-control input-lg')); ?>
								<p class="note">The full title of the article.</p>
							</div>

							<div class="form-group">
								<label>Article</label>
								<?php echo $this->Form->input('Article.content', array('class'=>'rte form-control input-lg tall')); ?>
								<p class="note">The content of the article.</p>
							</div>

							<div class="form-group">
								<div class="checkbox">
									<label>
										<?php echo $this->Form->input('Article.active', array('checked'=>true, 'class'=>'checkbox style-0')); ?>
										<span>Visible</span>
									</label>
								</div>
								<p class="note">Whether or not this article is visible to users.</p>
							</div>

						</fieldset>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->Html->link('Cancel', array('controller'=>'articles', 'action'=>'index'), array('class'=>'btn btn-default')); ?>
									<?php echo $this->Form->submit('Save Changes', array('class'=>'btn btn-primary', 'div'=>false)); ?>
								</div>
							</div>
						</div>
					<?php
					echo $this->Form->end();
					?>

				</div>

			</div>

		</div>

	</article>
</div>

<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
		<?php echo $this->Html->link('Delete this Article', array('controller'=>'articles', 'action'=>'delete', $article['Article']['id']), array(), "Are you sure you want to delete this article?"); ?>
	</article>

</div>