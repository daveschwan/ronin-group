
<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-group fa-fw "></i>
			Users
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading" class="drk">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2>User List</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th><?php echo $this->Paginator->sort('User.name', 'Name'); ?></th>
								<th><?php echo $this->Paginator->sort('User.email', 'Email'); ?></th>
								<th><?php echo $this->Paginator->sort('Role.name', 'Role'); ?></th>
								<th class="center"><?php echo $this->Paginator->sort('User.active', 'Active'); ?></th>
								<th class="center">Delete</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($users as $user) {
								?>
								<tr>
									<td><?php echo $this->Html->link($user['User']['name'], array('controller'=>'users', 'action'=>'edit', $user['User']['id'])); ?></td>
									<td><?php echo $this->Html->link($user['User']['email'], array('controller'=>'users', 'action'=>'edit', $user['User']['id'])); ?></td>
									<td><?php echo $user['Role']['name']; ?></td>
									<td class="center">
										<?php
										$icon = ($user['User']['active']) ? 'fa-check' : 'fa-minus';
										echo '<i class="fa ' . $icon . ' fa-lg"></i>'
										?>
									</td>
									<td class="center">
										<?php
										echo $this->Html->link('<i class="fa fa-times fa-lg"></i>', array('controller'=>'users', 'action'=>'delete', $user['User']['id']), array('escape'=>false), 'Are you sure you want to delete the user: "' . $user['User']['name'] . '"?');
										?>
									</td>
								</tr>
							<?php
							}
							?>
						</tbody>
					</table>

				</div>

			</div>

		</div>

	</article>
</div>