
<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-user fa-fw "></i>
			<?php echo $this->Html->link('Users', array('controller'=>'users', 'action'=>'index')); ?>
			<span>&nbsp;&nbsp; &gt; &nbsp;&nbsp;</span>
			Add
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2>General User Info</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<?php
					echo $this->Form->create('User', array('inputDefaults'=>array('label'=>false,'div'=>false)));
						?>
						<fieldset>

							<div class="form-group">
								<label>Name</label>
								<?php echo $this->Form->input('User.name', array('class'=>'form-control input-lg')); ?>
								<p class="note">The full name (not 'username') of the user.  Example: John Doe"</p>
							</div>

							<div class="form-group">
								<label>Role</label>
								<?php echo $this->Form->input('User.role_id', array('type'=>'select', 'class'=>'form-control input-lg', 'default'=>'18DD8261-AF42-4D16-B54C-E43808BB42F7')); ?>
								<p class="note">Allows the user to access varying aspects of the website.  If you're not sure, select "User".</p>
							</div>

							<div class="form-group">
								<label>Email Address</label>
								<?php echo $this->Form->input('User.email', array('class'=>'form-control input-lg')); ?>
								<p class="note">The full email address of the user.  IMPORTANT: This will be used for their log-in.</p>
							</div>

							<div class="form-group">
								<label>Password</label>
								<?php echo $this->Form->input('User.password', array('class'=>'input-xxlarge form-control')); ?>
								<p class="note">Must be at least 8 characters in length.</p>
							</div>

							<div class="form-group">
								<label>Verify Password</label>
								<?php echo $this->Form->input('User.re_password', array('type'=>'password', 'class'=>'input-xxlarge form-control')); ?>
								<p class="note">Must exactly match the "Password".</p>
							</div>

						</fieldset>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->Html->link('Cancel', array('controller'=>'users', 'action'=>'index'), array('class'=>'btn btn-default')); ?>
									<?php echo $this->Form->submit('Add User', array('class'=>'btn btn-primary', 'div'=>false)); ?>
								</div>
							</div>
						</div>

					<?php
					echo $this->Form->end();
					?>

				</div>

			</div>

		</div>

	</article>
</div>