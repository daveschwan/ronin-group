
<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-user fa-fw "></i>
			<?php echo $this->Html->link('Users', array('controller'=>'users', 'action'=>'index')); ?>
<!-- 			<span>&nbsp;&nbsp; &gt; &nbsp;&nbsp;</span>
			Edit -->
			<span>&nbsp;&nbsp; &gt; &nbsp;&nbsp;</span>
			<?php echo $user['Profile']['name']; ?>
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading">
				<span class="widget-icon"> <i class="fa fa-plus"></i> </span>
				<h2>General User Info</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<?php
					echo $this->Form->create('User', array('inputDefaults'=>array('label'=>false,'div'=>false)));
						echo $this->Form->input('User.id');
						?>


						<fieldset>

							<div class="form-group">
								<label>Name</label>
								<?php echo $this->Form->input('Profile.name', array('class'=>'form-control input-lg')); ?>
								<p class="note">The full name (not 'username') of the user.  Example: John Doe"</p>
							</div>

							<div class="form-group">
								<label>Email Address</label>
								<?php echo $this->Form->input('User.email', array('class'=>'form-control input-lg')); ?>
								<p class="note">The full email address of the user.  IMPORTANT: This will be used for their log-in.</p>
							</div>

						</fieldset>


						<fieldset>

							<div id="edit-password-area" style="display:none;">
								<div class="form-group">
									<label>Password</label>
									<?php echo $this->Form->input('User.password', array('class'=>'input-xxlarge form-control', 'value'=>'', 'disabled'=>'disabled')); ?>
									<p class="note">Must be at least 8 characters in length.</p>
								</div>
								<div class="form-group">
									<label>Verify Password</label>
									<?php echo $this->Form->input('User.re_password', array('type'=>'password', 'class'=>'input-xxlarge form-control', 'value'=>'', 'disabled'=>'disabled')); ?>
									<p class="note">Must exactly match the "Password".</p>
								</div>
							</div>

							<div id="edit-password-button-area">
								<a href="javascript:showPasswordArea();">Edit Password</a>
							</div>

							<div id="dont-edit-password-button-area" style="display:none;">
								<a href="javascript:hidePasswordArea();">Don't Edit Password</a>
							</div>

						</fieldset>



						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->Html->link('Cancel', array('controller'=>'users', 'action'=>'index'), array('class'=>'btn btn-default')); ?>
									<?php echo $this->Form->submit('Save Changes to User', array('class'=>'btn btn-primary', 'div'=>false)); ?>
								</div>
							</div>
						</div>

					<?php
					$this->Form->unlockField('User.password');
					$this->Form->unlockField('User.re-password');
					echo $this->Form->end();
					?>

				</div>

			</div>

		</div>

	</article>
</div>

<script>
	function showPasswordArea() {
		$('#edit-password-area').show();
		$('#edit-password-area input').removeAttr('disabled');
		$('#dont-edit-password-button-area').show();
		$('#edit-password-button-area').hide();
		$('#UserPassword').focus();
	}
	function hidePasswordArea() {
		$('#edit-password-area').hide();
		$('#edit-password-area input').prop('disabled', 'disabled');
		$('#dont-edit-password-button-area').hide();
		$('#edit-password-button-area').show();
	}
</script>