<?php
$icon = 'icon-ok-sign';
if(isset($type)) {
	switch($type) {
		case 'fail':
			$type='danger';
		case 'danger':
			$icon = 'icon-warning-sign';
			break;
		case 'info':
			$icon = 'icon-info';
			break;
		case 'success':
			$icon = 'icon-ok-sign';
			break;
		case 'error':
			$type = 'danger';
			$icon = 'icon-warning-sign';
			break;
	}
}

?>
<div class="alert alert-<?php echo $type ?>">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<i class="<?php echo $icon ?>"></i> <?php echo $message; ?>
</div>