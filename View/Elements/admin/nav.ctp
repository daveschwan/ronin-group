<?php

//CONFIGURATION for SmartAdmin UI

//ribbon breadcrumbs config
//array("Display Name" => "URL");
// $breadcrumbs = array(
// 	"Home" => APP_URL
// );

/*navigation array config

ex:
"dashboard" => array(
	"title" => "Display Title",
	"url" => "http://yoururl.com",
	"icon" => "fa-home"
	"label_htm" => "<span>Add your custom label/badge html here</span>",
	"sub" => array() //contains array of sub items with the same format as the parent
)

*/
$page_nav = array(
	"articles" => array(
		"title" => "Articles",
		"icon" => "fa-book",
		"sub" => array(
			"add-article" => array(
				"title" => "Write an Article",
				"url" => $this->Html->url(array('controller'=>'articles', 'action'=>'add')),
				"icon" => "fa-plus"
			),
			"articles" => array(
				"title" => "View All Articles",
				"url" => $this->Html->url(array('controller'=>'articles', 'action'=>'index')),
				"icon" => "fa-folder-open",
			),
		)
	),
	"courses" => array(
		"title" => "Courses",
		"icon" => "fa-calendar",
		"sub" => array(
			"courses" => array(
				"title" => "List Courses",
				"url" => $this->Html->url(array('controller'=>'courses', 'action'=>'index')),
				"icon" => "fa-list-ul"
			),
			"add-course" => array(
				"title" => "Add a Course",
				"url" => $this->Html->url(array('controller'=>'courses', 'action'=>'add')),
				"icon" => "fa-plus"
			),
			"instructors" => array(
				"title" => "Instructors",
				"icon" => "fa-group",
				"sub" => array(
					"list-instructors" => array(
						"title" => "List Instructors",
						"url" => $this->Html->url(array('controller'=>'instructors', 'action'=>'index')),
						"icon" => "fa-list-ul"
					),
					"add-instructors" => array(
						"title" => "Add an Instructor",
						"url" => $this->Html->url(array('controller'=>'instructors', 'action'=>'add')),
						"icon" => "fa-plus"
					)
				)
			),
		)
	),
	"merchandise" => array(
		"title" => "Merchandise",
		"icon" => "fa-usd",
		"sub" => array(
			"sales" => array(
				"title" => "Orders",
				"url" => $this->Html->url(array('controller'=>'orders', 'action'=>'index')),
				"icon" => "fa-list-ul"
			),
			"reporting" => array(
				"title" => "Reporting",
				"url" => $this->Html->url(array('controller'=>'orders', 'action'=>'reports')),
				"icon" => "fa-bar-chart-o"
			),
			// "items" => array(
			// 	"title" => "Items",
			// 	"icon" => "fa-trophy",
			// 	"sub" => array(
			// 		"list-items" => array(
			// 			"title" => "List Items",
			// 			"url" => $this->Html->url(array('controller'=>'merchandise', 'action'=>'index')),
			// 			"icon" => "fa-list-ul",
			// 		),
			// 		"add-item" => array(
			// 			"title" => "Add an Item",
			// 			"url" => $this->Html->url(array('controller'=>'merchandise', 'action'=>'add')),
			// 			"icon" => "fa-plus",
			// 		),
			// 	)
			// ),
			"types" => array(
				"title" => "Merchandises",
				"icon" => "fa-trophy",
				"sub" => array(
					"merchandise-types" => array(
						"title" => "List All",
						"url" => $this->Html->url(array('controller'=>'merchandises', 'action'=>'index')),
						"icon" => "fa-list-ul",
					),
					"add-merchandise-type" => array(
						"title" => "Add an Item",
						"url" => $this->Html->url(array('controller'=>'merchandises', 'action'=>'add')),
						"icon" => "fa-plus",
					),
				)
			)
		)
	),
	"users" => array(
		"title" => "Users",
		"icon" => "fa-user",
		"sub" => array(
			"users" => array(
				"title" => "List Users",
				"url" => $this->Html->url(array('controller'=>'users', 'action'=>'index')),
				"icon" => "fa-group",
			),
			"add-user" => array(
				"title" => "Add a User",
				"url" => $this->Html->url(array('controller'=>'users', 'action'=>'add')),
				"icon" => "fa-plus"
			),
		)
	),
	"examples" => array(
		"title" => "Examples",
		"icon" => "fa-home",
		"sub" => array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'index')),
				"icon" => "fa-home"
			),
			"inbox" => array(
				"title" => "Inbox",
				"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'inbox')),
				"icon" => "fa-inbox",
				"label_htm" => '<span class="badge pull-right inbox-badge">14</span>'
			),
			"graphs" => array(
				"title" => "Graphs",
				"icon" => "fa-bar-chart-o",
				"sub" => array(
					"flot" => array(
						"title" => "Flot Chart",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'flot'))
					),
					"morris" => array(
						"title" => "Morris Charts",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'morris'))
					),
					"inline" => array(
						"title" => "Inline Charts",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'inline_charts'))
					)
				)
			),
			"tables" => array(
				"title" => "Tables",
				"icon" => "fa-table",
				"sub" => array(
					"normal" => array(
						"title" => "Normal Tables",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'table'))
					),
					"data" => array(
						"title" => "Data Tables",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'datatables'))
					)
				)
			),
			"forms" => array(
				"title" => "Forms",
				"icon" => "fa-pencil-square-o",
				"sub" => array(
					"smart_elements" => array(
						"title" => "Smart Form Elements",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'form_elements'))
					),
		            "smart_layout" => array(
						"title" => "Smart Form Layouts",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'form_templates'))
					),
		            "smart_validation" => array(
						"title" => "Smart Form Validation",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'validation'))
					),
		            "bootstrap_forms" => array(
						"title" => "Bootstrap Form Elements",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'bootstrap_forms'))
					),
		            "form_plugins" => array(
						"title" => "Form Plugins",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'plugins'))
					),
		            "wizards" => array(
						"title" => "Wizards",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'wizard'))
					),
		            "bootstrap_editors" => array(
						"title" => "Bootstrap Editors",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'other_editors'))
					),
		            "dropzone" => array(
						"title" => "Dropzone",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'dropzone')),
		                "label_htm" => '<span class="badge pull-right inbox-badge bg-color-yellow">new</span>'
					)
				)
			),
		    "ui_elements" => array(
		        "title" => "UI Elements",
		        "icon" => "fa-desktop",
		        "sub" => array(
		            "general" => array(
		                "title" => "General Elements",
		                "url" => $this->Html->url(array('controller'=>'examples', 'action'=>'general_elements'))
		            ),
		            "buttons" => array(
		                "title" => "Buttons",
		                "url" => $this->Html->url(array('controller'=>'examples', 'action'=>'buttons'))
		            ),
		            "icons" => array(
		                "title" => "Icons",
		                "sub" => array(
		                    "fa" => array(
		                        "title" => "Font Awesome",
		                        "icon" => "fa-plane",
		                        "url" => $this->Html->url(array('controller'=>'examples', 'action'=>'fa'))
		                    ),
		                    "glyph" => array(
		                        "title" => "Glyph Icons",
		                        "icon" => "fa-plane",
		                        "url" => $this->Html->url(array('controller'=>'examples', 'action'=>'glyph'))
		                    )
		                )
		            ),
		            "grid" => array(
		                "title" => "Grid",
		                "url" => $this->Html->url(array('controller'=>'examples', 'action'=>'grid'))
		            ),
		            "tree_view" => array(
		                "title" => "Tree View",
		                "url" => $this->Html->url(array('controller'=>'examples', 'action'=>'treeview'))
		            ),
		            "nestable_lists" => array(
		                "title" => "Nestable Lists",
		                "url" => $this->Html->url(array('controller'=>'examples', 'action'=>'nestable_list'))
		            ),
		            "jquery_ui" => array(
		                "title" => "jQuery UI",
		                "url" => $this->Html->url(array('controller'=>'examples', 'action'=>'jqui'))
		            )
		        )
		    ),
			"nav6" => array(
				"title" => "6 Level Navigation",
				"icon" => "fa-folder-open",
				"sub" => array(
					"second_lvl" => array(
						"title" => "2nd Level",
						"icon" => "fa-folder-open",
						"sub" => array(
							"third_lvl" => array(
								"title" => "3rd Level",
								"icon" => "fa-folder-open",
								"sub" => array(
									"file" => array(
										"title" => "File",
										"icon" => "fa-file-text"
									),
									"fourth_lvl" => array(
										"title" => "4th Level",
										"icon" => "fa-folder-open",
										"sub" => array(
											"file" => array(
												"title" => "File",
												"icon" => "fa-file-text"
											),
											"fifth_lvl" => array(
												"title" => "5th Level",
												"icon" => "fa-folder-open",
												"sub" => array(
													"file" => array(
														"title" => "File",
														"icon" => "fa-file-text"
													),
													"file" => array(
														"title" => "File",
														"icon" => "fa-file-text"
													)
												)
											)
										)
									)
								)
							)
						)
					),
					"folder" => array(
						"title" => "Folder",
						"icon" => "fa-folder-open",
						"sub" => array(
							"third_lvl" => array(
								"title" => "3rd Level",
								"icon" => "fa-folder-open",
								"sub" => array(
									"file1" => array(
										"title" => "File",
										"icon" => "fa-file-text"
									),
									"file2" => array(
										"title" => "File",
										"icon" => "fa-file-text"
									)
								)
							)
						)
					)
				)
			),
		    "cal" => array(
				"title" => "Calendar",
				"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'calendar')),
				"icon" => "fa-calendar",
				"icon_badge" => "3"
			),
		    "widgets" => array(
				"title" => "Widgets",
				"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'widgets')),
				"icon" => "fa-list-alt"
			),
		    "gallery" => array(
				"title" => "Gallery",
				"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'gallery')),
				"icon" => "fa-picture-o"
			),
		    "gmap_skins" => array(
				"title" => "Google Map Skins",
				"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'gmap-xml')),
				"icon" => "fa-map-marker",
		        "label_htm" => '<span class="badge bg-color-greenLight pull-right inbox-badge">9</span>'
			),
			"misc" => array(
				"title" => "Miscellaneous",
				"icon" => "fa-windows",
				"sub" => array(
		            "typo" => array(
						"title" => "Typography",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'typography'))
					),
		            "pricing_tables" => array(
						"title" => "Pricing Tables",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'pricing_table'))
					),
		            "invoice" => array(
						"title" => "Invoice",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'invoice'))
					),
		            "login" => array(
						"title" => "Login",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'login'))
					),
		            "register" => array(
						"title" => "Register",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'register'))
					),
		            "lock" => array(
						"title" => "Lock Screen",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'lock'))
					),
		            "err_404" => array(
						"title" => "Error 404",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'error404'))
					),
		            "err_500" => array(
						"title" => "Error 500",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'error500'))
					),
					"blank" => array(
						"title" => "Blank Page",
						"icon" => "fa-file",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'blank_'))
					),
		            "email_template" => array(
						"title" => "Email Template",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'email_template'))
					),
		            "search" => array(
						"title" => "Search Page",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'search'))
					),
		            "ck_editor" => array(
						"title" => "CK Editor",
						"url" => $this->Html->url(array('controller'=>'examples', 'action'=>'ckeditor'))
					),
				)
			),
		    "others" => array(
		        "title" => "Other Pages",
		        "icon" => "fa-file",
		        "sub" => array(
		            "forum" => array(
		                "title" => "Forum Layout",
		                "url" => $this->Html->url(array('controller'=>'examples', 'action'=>'forum'))
		            ),
		            "profile" => array(
		                "title" => "Profile",
		                "url" => $this->Html->url(array('controller'=>'examples', 'action'=>'profile'))
		            ),
		            "timeline" => array(
		                "title" => "Timeline",
		                "url" => $this->Html->url(array('controller'=>'examples', 'action'=>'timeline'))
		            )
		        )
		    )
		)
	)
);

//configuration variables
$page_title = "";
$no_main_header = false; //set true for lock.php and login.php
$page_body_prop = array(); //optional properties for <body>
?>


<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">

	<!-- User info -->
	<div class="login-info">
		<span> <!-- User image size is adjusted inside CSS, it should stay as it -->

			<a href="javascript:void(0);" id="show-shortcut">
				<img src="/img/avatars/sunny.png" alt="me" class="online" />
				<span>
					<?php echo $this->Text->truncate(str_replace(' ', '.', $currentUser['name']), 30); ?>
				</span>
				<i class="fa fa-angle-down"></i>
			</a>

		</span>
	</div>
	<!-- end user info -->

	<!-- NAVIGATION : This navigation is also responsive

	To make this navigation dynamic please make sure to link the node
	(the reference to the nav > ul) after page load. Or the navigation
	will not initialize.
	-->
	<nav>
		<!-- NOTE: Notice the gaps after each icon usage <i></i>..
		Please note that these links work a bit different than
		traditional hre="" links. See documentation for details.
		-->
		<ul>
			<?php
				foreach ($page_nav as $key => $nav_item) {
					//process parent nav
					$nav_htm = '';
					$url = isset($nav_item["url"]) ? $nav_item["url"] : "#";
					$icon_badge = isset($nav_item["icon_badge"]) ? '<em>'.$nav_item["icon_badge"].'</em>' : '';
					$icon = isset($nav_item["icon"]) ? '<i class="fa fa-lg fa-fw '.$nav_item["icon"].'">'.$icon_badge.'</i>' : "";
					$nav_title = isset($nav_item["title"]) ? $nav_item["title"] : "(No Name)";
					$label_htm = isset($nav_item["label_htm"]) ? $nav_item["label_htm"] : "";
					$nav_htm .= '<a href="'.$url.'" title="'.$nav_title.'">'.$icon.' <span class="menu-item-parent">'.$nav_title.'</span>'.$label_htm.'</a>';

					if (isset($nav_item["sub"]) && $nav_item["sub"])
						$nav_htm .= process_sub_nav($nav_item["sub"]);

					echo '<li '.(isset($nav_item["active"]) ? 'class = "active"' : '').'>'.$nav_htm.'</li>';
				}

				function process_sub_nav($nav_item) {
					$sub_item_htm = "";
					if (isset($nav_item["sub"]) && $nav_item["sub"]) {
						$sub_nav_item = $nav_item["sub"];
						$sub_item_htm = process_sub_nav($sub_nav_item);
					} else {
						$sub_item_htm .= '<ul>';
						foreach ($nav_item as $key => $sub_item) {
							$url = isset($sub_item["url"]) ? $sub_item["url"] : "#";
							$icon = isset($sub_item["icon"]) ? '<i class="fa fa-lg fa-fw '.$sub_item["icon"].'"></i>' : "";
							$nav_title = isset($sub_item["title"]) ? $sub_item["title"] : "(No Name)";
							$label_htm = isset($sub_item["label_htm"]) ? $sub_item["label_htm"] : "";
							$sub_item_htm .=
								'<li '.(isset($sub_item["active"]) ? 'class = "active"' : '').'>
									<a href="'.$url.'">'.$icon.' '.$nav_title.$label_htm.'</a>
									'.(isset($sub_item["sub"]) ? process_sub_nav($sub_item["sub"]) : '').'
								</li>';
						}
						$sub_item_htm .= '</ul>';
					}
					return $sub_item_htm;
				}


			?>
		</ul>

	</nav>
	<span class="minifyme"> <i class="fa fa-arrow-circle-left hit"></i> </span>

</aside>
<!-- END NAVIGATION -->