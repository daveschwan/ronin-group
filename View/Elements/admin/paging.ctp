<ul class="pagination pagination-md pull-right">
	<?php
	echo $this->Paginator->prev(
		'<i class="fa fa-chevron-left"></i>',
		array(
			'escape'=>false,
			'tag'=>'li',
			'disabledTag'=>'a'
		)
	);
	echo $this->Paginator->numbers(array(
		'modulus' => 10, //numbers to have on each side of current page
		'first' => 1, //link to the first page (or number of "first" links)
		'last' => 1, //link to the last page (or number of "last" links)
		'tag' => 'li',
		'currentTag' => 'a',
		// 'class' => 'number',
		'currentClass' => 'active', //class of the current page's link
		'separator' => '',
	));
	echo $this->Paginator->next(
		'<i class="fa fa-chevron-right"></i>',
		array(
			'escape'=>false,
			'tag'=>'li',
			'disabledTag'=>'a'
		)
	);
	?>
</ul>
<div style="clear:both;"></div>