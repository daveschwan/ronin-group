<?php
class StringHelper extends AppHelper {

    /**
     * Prepends a string with "a " or "an "
     * @param  [string] $string the string to prepend
     * @return [string]         [the prepended string]
     */
    public function aOrAn($string) {
        $vowels = array('a','e','i','o','u');
        if(in_array(substr(strtolower($string),0,1), $vowels)) {
                return 'an ' . $string;
        } else {
                return 'a ' . $string;
        }
    }

}