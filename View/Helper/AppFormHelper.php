<?php
App::uses('FormHelper', 'View/Helper');
class AppFormHelper extends FormHelper {


/**
 * Makes the default for multiple-select checkboxes instead of multi-select
 * @param  [type] $fieldName
 * @param  array  $options
 * @param  array  $attributes
 * @return [type]
 */
    public function select($fieldName, $options = array(), $attributes = array()) {
        if(!empty($attributes['multiple'])) {
            $attributes['multiple'] = 'checkbox';
        }
        return parent::select($fieldName, $options, $attributes);
    }

}