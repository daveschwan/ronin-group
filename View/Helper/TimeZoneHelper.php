<?php
class TimeZoneHelper extends AppHelper {

	public $helpers = array('Form');

/**
 * Displays a select box of timezones
 * @param  [type] $fieldname [description]
 * @param  string $label     [description]
 * @return [type]            [description]
 */
	  public function select_old($fieldname, $label="Please Choose a timezone") {
			$list = $this->Form->input($fieldname, array("type"=>"select", "label"=>$label, "options"=>$this->timezones, "empty"=>"Please choose a timezone"));
			return $this->output($list);
	  }



/**
 * [get_timezone_abbreviation description]
 * @param  [type] $timezone_id [description]
 * @return [type]              [description]
 */
	  function get_timezone_abbreviation($timezone_id) {
		  if($timezone_id){
			  $abb_list = timezone_abbreviations_list();

			  $abb_array = array();
			  foreach ($abb_list as $abb_key => $abb_val) {
				  foreach ($abb_val as $key => $value) {
					  $value['abb'] = $abb_key;
					  array_push($abb_array, $value);
				  }
			  }

			  foreach ($abb_array as $key => $value) {
				  if($value['timezone_id'] == $timezone_id){
					  return strtoupper($value['abb']);
				  }
			  }
		  }
		  return false;
	  }



	public function select($options = array()) {
		$defaults = array(
			'name'     => 'timezone',
			'class'    => 'form-control',
			'id'       => 'timezones',
			'selected' => 'America/New_York'
		);
		extract(array_merge($defaults, $options));
		$toReturn = '<select name="' . $name . '" id="' . $id . '" class="' . $class . '">';
		$usOptions = array(
			'Pacific/Honolulu' => '(GMT-1000) United States (Honolulu) Time',
			'America/Los_Angeles' => '(GMT-0800) United States (Los Angeles) Time',
			'America/Denver' => '(GMT-0700) United States (Denver) Time',
			'America/Phoenix' => '(GMT-0700) United States (Phoenix) Time',
			'America/Chicago' => '(GMT-0600) United States (Chicago) Time',
			'America/New_York' => '(GMT-0500) United States (New York) Time',
		);
		$restOptions = array(
			'Pacific/Pago_Pago' => '(GMT-1100) American Samoa Time',
			'Pacific/Niue' => '(GMT-1100) Niue Time',
			'Pacific/Midway' => '(GMT-1100) United States Minor Outlying Islands (Midway) Time',
			'Pacific/Rarotonga' => '(GMT-1000) Cook Islands Time',
			'Pacific/Tahiti' => '(GMT-1000) French Polynesia (Tahiti) Time',
			// 'Pacific/Honolulu' => '(GMT-1000) United States (Honolulu) Time',
			'Pacific/Johnston' => '(GMT-1000) United States Minor Outlying Islands (Johnston) Time',
			'Pacific/Marquesas' => '(GMT-0930) French Polynesia (Marquesas) Time',
			'Pacific/Gambier' => '(GMT-0900) French Polynesia (Gambier) Time',
			'America/Anchorage' => '(GMT-0900) United States (Anchorage) Time',
			'America/Vancouver' => '(GMT-0800) Canada (Vancouver) Time',
			'America/Whitehorse' => '(GMT-0800) Canada (Whitehorse) Time',
			'America/Tijuana' => '(GMT-0800) Mexico (Tijuana) Time',
			'Pacific/Pitcairn' => '(GMT-0800) Pitcairn Time',
			// 'America/Los_Angeles' => '(GMT-0800) United States (Los Angeles) Time',
			'America/Dawson_Creek' => '(GMT-0700) Canada (Dawson Creek) Time',
			'America/Edmonton' => '(GMT-0700) Canada (Edmonton) Time',
			'America/Yellowknife' => '(GMT-0700) Canada (Yellowknife) Time',
			'America/Hermosillo' => '(GMT-0700) Mexico (Hermosillo) Time',
			'America/Mazatlan' => '(GMT-0700) Mexico (Mazatlan) Time',
			// 'America/Denver' => '(GMT-0700) United States (Denver) Time',
			// 'America/Phoenix' => '(GMT-0700) United States (Phoenix) Time',
			'America/Belize' => '(GMT-0600) Belize Time',
			'America/Regina' => '(GMT-0600) Canada (Regina) Time',
			'America/Winnipeg' => '(GMT-0600) Canada (Winnipeg) Time',
			'America/Costa_Rica' => '(GMT-0600) Costa Rica Time',
			'Pacific/Galapagos' => '(GMT-0600) Ecuador (Galapagos) Time',
			'America/El_Salvador' => '(GMT-0600) El Salvador Time',
			'America/Guatemala' => '(GMT-0600) Guatemala Time',
			'America/Tegucigalpa' => '(GMT-0600) Honduras Time',
			'America/Mexico_City' => '(GMT-0600) Mexico (Mexico City) Time',
			'America/Managua' => '(GMT-0600) Nicaragua Time',
			// 'America/Chicago' => '(GMT-0600) United States (Chicago) Time',
			'America/Nassau' => '(GMT-0500) Bahamas Time',
			'America/Iqaluit' => '(GMT-0500) Canada (Iqaluit) Time',
			'America/Montreal' => '(GMT-0500) Canada (Montreal) Time',
			'America/Toronto' => '(GMT-0500) Canada (Toronto) Time',
			'America/Cayman' => '(GMT-0500) Cayman Islands Time',
			'Pacific/Easter' => '(GMT-0500) Chile (Easter) Time',
			'America/Bogota' => '(GMT-0500) Colombia Time',
			'America/Havana' => '(GMT-0500) Cuba Time',
			'America/Guayaquil' => '(GMT-0500) Ecuador (Guayaquil) Time',
			'America/Port-au-Prince' => '(GMT-0500) Haiti Time',
			'America/Jamaica' => '(GMT-0500) Jamaica Time',
			'America/Panama' => '(GMT-0500) Panama Time',
			'America/Lima' => '(GMT-0500) Peru Time',
			'America/Grand_Turk' => '(GMT-0500) Turks and Caicos Islands Time',
			// 'America/New_York' => '(GMT-0500) United States (New York) Time',
			'America/Caracas' => '(GMT-0430) Venezuela Time',
			'America/Anguilla' => '(GMT-0400) Anguilla Time',
			'America/Antigua' => '(GMT-0400) Antigua and Barbuda Time',
			'America/Aruba' => '(GMT-0400) Aruba Time',
			'America/Barbados' => '(GMT-0400) Barbados Time',
			'Atlantic/Bermuda' => '(GMT-0400) Bermuda Time',
			'America/La_Paz' => '(GMT-0400) Bolivia Time',
			'America/Boa_Vista' => '(GMT-0400) Brazil (Boa Vista) Time',
			'America/Manaus' => '(GMT-0400) Brazil (Manaus) Time',
			'America/Porto_Velho' => '(GMT-0400) Brazil (Porto Velho) Time',
			'America/Rio_Branco' => '(GMT-0400) Brazil (Rio Branco) Time',
			'America/Tortola' => '(GMT-0400) British Virgin Islands Time',
			'America/Halifax' => '(GMT-0400) Canada (Halifax) Time',
			'America/Dominica' => '(GMT-0400) Dominica Time',
			'America/Santo_Domingo' => '(GMT-0400) Dominican Republic Time',
			'America/Thule' => '(GMT-0400) Greenland (Thule) Time',
			'America/Grenada' => '(GMT-0400) Grenada Time',
			'America/Guadeloupe' => '(GMT-0400) Guadeloupe Time',
			'America/Guyana' => '(GMT-0400) Guyana Time',
			'America/Martinique' => '(GMT-0400) Martinique Time',
			'America/Montserrat' => '(GMT-0400) Montserrat Time',
			'America/Curacao' => '(GMT-0400) Netherlands Antilles Time',
			'America/Puerto_Rico' => '(GMT-0400) Puerto Rico Time',
			'America/St_Kitts' => '(GMT-0400) Saint Kitts and Nevis Time',
			'America/St_Lucia' => '(GMT-0400) Saint Lucia Time',
			'America/St_Vincent' => '(GMT-0400) Saint Vincent and the Grenadines Time',
			'America/Port_of_Spain' => '(GMT-0400) Trinidad and Tobago Time',
			'America/St_Thomas' => '(GMT-0400) U.S. Virgin Islands Time',
			'America/St_Johns' => '(GMT-0330) Canada (St. John\'s) Time',
			'Antarctica/Palmer' => '(GMT-0300) Antarctica (Palmer) Time',
			'Antarctica/Rothera' => '(GMT-0300) Antarctica (Rothera) Time',
			'America/Argentina/Buenos_Aires' => '(GMT-0300) Argentina (Buenos Aires) Time',
			'America/Bahia' => '(GMT-0300) Brazil (Bahia) Time',
			'America/Belem' => '(GMT-0300) Brazil (Belem) Time',
			'America/Campo_Grande' => '(GMT-0300) Brazil (Campo Grande) Time',
			'America/Cuiaba' => '(GMT-0300) Brazil (Cuiaba) Time',
			'America/Fortaleza' => '(GMT-0300) Brazil (Fortaleza) Time',
			'America/Maceio' => '(GMT-0300) Brazil (Maceio) Time',
			'America/Recife' => '(GMT-0300) Brazil (Recife) Time',
			'America/Santiago' => '(GMT-0300) Chile (Santiago) Time',
			'Atlantic/Stanley' => '(GMT-0300) Falkland Islands Time',
			'America/Cayenne' => '(GMT-0300) French Guiana Time',
			'America/Godthab' => '(GMT-0300) Greenland (Godthab) Time',
			'America/Asuncion' => '(GMT-0300) Paraguay Time',
			'America/Miquelon' => '(GMT-0300) Saint Pierre and Miquelon Time',
			'America/Paramaribo' => '(GMT-0300) Suriname Time',
			'America/Araguaina' => '(GMT-0200) Brazil (Araguaina) Time',
			'America/Noronha' => '(GMT-0200) Brazil (Noronha) Time',
			'America/Sao_Paulo' => '(GMT-0200) Brazil (Sao Paulo) Time',
			'Atlantic/South_Georgia' => '(GMT-0200) South Georgia and the South Sandwich Islands Time',
			'America/Montevideo' => '(GMT-0200) Uruguay Time',
			'Atlantic/Cape_Verde' => '(GMT-0100) Cape Verde Time',
			'America/Scoresbysund' => '(GMT-0100) Greenland (Scoresbysund) Time',
			'Atlantic/Azores' => '(GMT-0100) Portugal (Azores) Time',
			'Africa/Ouagadougou' => '(GMT+0000) Burkina Faso Time',
			'Atlantic/Faroe' => '(GMT+0000) Faroe Islands Time',
			'Africa/Banjul' => '(GMT+0000) Gambia Time',
			'Africa/Accra' => '(GMT+0000) Ghana Time',
			'America/Danmarkshavn' => '(GMT+0000) Greenland (Danmarkshavn) Time',
			'Africa/Conakry' => '(GMT+0000) Guinea Time',
			'Africa/Bissau' => '(GMT+0000) Guinea-Bissau Time',
			'Atlantic/Reykjavik' => '(GMT+0000) Iceland Time',
			'Europe/Dublin' => '(GMT+0000) Ireland Time',
			'Africa/Abidjan' => '(GMT+0000) Ivory Coast Time',
			'Africa/Monrovia' => '(GMT+0000) Liberia Time',
			'Africa/Bamako' => '(GMT+0000) Mali Time',
			'Africa/Nouakchott' => '(GMT+0000) Mauritania Time',
			'Africa/Casablanca' => '(GMT+0000) Morocco Time',
			'Europe/Lisbon' => '(GMT+0000) Portugal (Lisbon) Time',
			'Atlantic/St_Helena' => '(GMT+0000) Saint Helena Time',
			'Africa/Sao_Tome' => '(GMT+0000) Sao Tome and Principe Time',
			'Africa/Dakar' => '(GMT+0000) Senegal Time',
			'Africa/Freetown' => '(GMT+0000) Sierra Leone Time',
			'Atlantic/Canary' => '(GMT+0000) Spain (Canary) Time',
			'Africa/Lome' => '(GMT+0000) Togo Time',
			'Europe/London' => '(GMT+0000) United Kingdom Time',
			'Africa/El_Aaiun' => '(GMT+0000) Western Sahara Time',
			'Etc/GMT' => '(GMT+0000) World (GMT) Time',
			'Europe/Tirane' => '(GMT+0100) Albania Time',
			'Africa/Algiers' => '(GMT+0100) Algeria Time',
			'Europe/Andorra' => '(GMT+0100) Andorra Time',
			'Africa/Luanda' => '(GMT+0100) Angola Time',
			'Europe/Vienna' => '(GMT+0100) Austria Time',
			'Europe/Brussels' => '(GMT+0100) Belgium Time',
			'Africa/Porto-Novo' => '(GMT+0100) Benin Time',
			'Africa/Douala' => '(GMT+0100) Cameroon Time',
			'Africa/Bangui' => '(GMT+0100) Central African Republic Time',
			'Africa/Ndjamena' => '(GMT+0100) Chad Time',
			'Africa/Brazzaville' => '(GMT+0100) Congo - Brazzaville Time',
			'Africa/Kinshasa' => '(GMT+0100) Congo - Kinshasa (Kinshasa) Time',
			'Europe/Prague' => '(GMT+0100) Czech Republic Time',
			'Europe/Copenhagen' => '(GMT+0100) Denmark Time',
			'Africa/Malabo' => '(GMT+0100) Equatorial Guinea Time',
			'Europe/Paris' => '(GMT+0100) France Time',
			'Africa/Libreville' => '(GMT+0100) Gabon Time',
			'Europe/Berlin' => '(GMT+0100) Germany Time',
			'Europe/Gibraltar' => '(GMT+0100) Gibraltar Time',
			'Europe/Budapest' => '(GMT+0100) Hungary Time',
			'Europe/Rome' => '(GMT+0100) Italy Time',
			'Europe/Vaduz' => '(GMT+0100) Liechtenstein Time',
			'Europe/Luxembourg' => '(GMT+0100) Luxembourg Time',
			'Europe/Malta' => '(GMT+0100) Malta Time',
			'Europe/Monaco' => '(GMT+0100) Monaco Time',
			'Europe/Amsterdam' => '(GMT+0100) Netherlands Time',
			'Africa/Niamey' => '(GMT+0100) Niger Time',
			'Africa/Lagos' => '(GMT+0100) Nigeria Time',
			'Europe/Oslo' => '(GMT+0100) Norway Time',
			'Europe/Warsaw' => '(GMT+0100) Poland Time',
			'Europe/Belgrade' => '(GMT+0100) Serbia Time',
			'Africa/Ceuta' => '(GMT+0100) Spain (Ceuta) Time',
			'Europe/Madrid' => '(GMT+0100) Spain (Madrid) Time',
			'Europe/Stockholm' => '(GMT+0100) Sweden Time',
			'Europe/Zurich' => '(GMT+0100) Switzerland Time',
			'Africa/Tunis' => '(GMT+0100) Tunisia Time',
			'Europe/Sofia' => '(GMT+0200) Bulgaria Time',
			'Asia/Nicosia' => '(GMT+0200) Cyprus Time',
			'Africa/Cairo' => '(GMT+0200) Egypt Time',
			'Europe/Tallinn' => '(GMT+0200) Estonia Time',
			'Europe/Helsinki' => '(GMT+0200) Finland Time',
			'Europe/Athens' => '(GMT+0200) Greece Time',
			'Asia/Jerusalem' => '(GMT+0200) Israel Time',
			'Asia/Amman' => '(GMT+0200) Jordan Time',
			'Europe/Riga' => '(GMT+0200) Latvia Time',
			'Asia/Beirut' => '(GMT+0200) Lebanon Time',
			'Europe/Vilnius' => '(GMT+0200) Lithuania Time',
			'Europe/Chisinau' => '(GMT+0200) Moldova Time',
			'Africa/Windhoek' => '(GMT+0200) Namibia Time',
			'Asia/Gaza' => '(GMT+0200) Palestinian Territory Time',
			'Europe/Bucharest' => '(GMT+0200) Romania Time',
			'Asia/Damascus' => '(GMT+0200) Syria Time',
			'Europe/Istanbul' => '(GMT+0200) Turkey Time',
			'Europe/Kiev' => '(GMT+0200) Ukraine (Kiev) Time',
			'Africa/Harare' => '(GMT+0200) Zimbabwe Time',
			'Antarctica/Syowa' => '(GMT+0300) Antarctica (Syowa) Time',
			'Asia/Bahrain' => '(GMT+0300) Bahrain Time',
			'Europe/Minsk' => '(GMT+0300) Belarus Time',
			'Indian/Comoro' => '(GMT+0300) Comoros Time',
			'Africa/Djibouti' => '(GMT+0300) Djibouti Time',
			'Africa/Asmara' => '(GMT+0300) Eritrea Time',
			'Africa/Addis_Ababa' => '(GMT+0300) Ethiopia Time',
			'Asia/Baghdad' => '(GMT+0300) Iraq Time',
			'Africa/Nairobi' => '(GMT+0300) Kenya Time',
			'Asia/Kuwait' => '(GMT+0300) Kuwait Time',
			'Indian/Antananarivo' => '(GMT+0300) Madagascar Time',
			'Indian/Mayotte' => '(GMT+0300) Mayotte Time',
			'Asia/Qatar' => '(GMT+0300) Qatar Time',
			'Europe/Kaliningrad' => '(GMT+0300) Russia (Kaliningrad) Time',
			'Asia/Riyadh' => '(GMT+0300) Saudi Arabia Time',
			'Africa/Mogadishu' => '(GMT+0300) Somalia Time',
			'Africa/Khartoum' => '(GMT+0300) Sudan Time',
			'Africa/Dar_es_Salaam' => '(GMT+0300) Tanzania Time',
			'Africa/Kampala' => '(GMT+0300) Uganda Time',
			'Asia/Aden' => '(GMT+0300) Yemen Time',
			'Asia/Tehran' => '(GMT+0330) Iran Time',
			'Asia/Yerevan' => '(GMT+0400) Armenia Time',
			'Asia/Baku' => '(GMT+0400) Azerbaijan Time',
			'Asia/Tbilisi' => '(GMT+0400) Georgia Time',
			'Indian/Mauritius' => '(GMT+0400) Mauritius Time',
			'Asia/Muscat' => '(GMT+0400) Oman Time',
			'Indian/Reunion' => '(GMT+0400) Reunion Time',
			'Europe/Moscow' => '(GMT+0400) Russia (Moscow) Time',
			'Europe/Samara' => '(GMT+0400) Russia (Samara) Time',
			'Indian/Mahe' => '(GMT+0400) Seychelles Time',
			'Asia/Dubai' => '(GMT+0400) United Arab Emirates Time',
			'Asia/Kabul' => '(GMT+0430) Afghanistan Time',
			'Antarctica/Mawson' => '(GMT+0500) Antarctica (Mawson) Time',
			'Indian/Kerguelen' => '(GMT+0500) French Southern Territories Time',
			'Asia/Aqtau' => '(GMT+0500) Kazakhstan (Aqtau) Time',
			'Asia/Aqtobe' => '(GMT+0500) Kazakhstan (Aqtobe) Time',
			'Indian/Maldives' => '(GMT+0500) Maldives Time',
			'Asia/Karachi' => '(GMT+0500) Pakistan Time',
			'Asia/Dushanbe' => '(GMT+0500) Tajikistan Time',
			'Asia/Ashgabat' => '(GMT+0500) Turkmenistan Time',
			'Asia/Tashkent' => '(GMT+0500) Uzbekistan (Tashkent) Time',
			'Asia/Calcutta' => '(GMT+0530) India Time',
			'Asia/Kolkata' => '(GMT+0530) India Time',
			'Asia/Colombo' => '(GMT+0530) Sri Lanka Time',
			'Asia/Katmandu' => '(GMT+0545) Nepal Time',
			'Antarctica/Vostok' => '(GMT+0600) Antarctica (Vostok) Time',
			'Asia/Dhaka' => '(GMT+0600) Bangladesh Time',
			'Asia/Thimphu' => '(GMT+0600) Bhutan Time',
			'Indian/Chagos' => '(GMT+0600) British Indian Ocean Territory Time',
			'Asia/Almaty' => '(GMT+0600) Kazakhstan (Almaty) Time',
			'Asia/Bishkek' => '(GMT+0600) Kyrgyzstan Time',
			'Asia/Yekaterinburg' => '(GMT+0600) Russia (Yekaterinburg) Time',
			'Indian/Cocos' => '(GMT+0630) Cocos Islands Time',
			'Asia/Rangoon' => '(GMT+0630) Myanmar Time',
			'Antarctica/Davis' => '(GMT+0700) Antarctica (Davis) Time',
			'Asia/Phnom_Penh' => '(GMT+0700) Cambodia Time',
			'Indian/Christmas' => '(GMT+0700) Christmas Island Time',
			'Asia/Jakarta' => '(GMT+0700) Indonesia (Jakarta) Time',
			'Asia/Vientiane' => '(GMT+0700) Laos Time',
			'Asia/Hovd' => '(GMT+0700) Mongolia (Hovd) Time',
			'Asia/Omsk' => '(GMT+0700) Russia (Omsk) Time',
			'Asia/Bangkok' => '(GMT+0700) Thailand Time',
			'Asia/Saigon' => '(GMT+0700) Vietnam Time',
			'Antarctica/Casey' => '(GMT+0800) Antarctica (Casey) Time',
			'Australia/Perth' => '(GMT+0800) Australia (Perth) Time',
			'Asia/Brunei' => '(GMT+0800) Brunei Time',
			'Asia/Shanghai' => '(GMT+0800) China (Shanghai) Time',
			'Asia/Hong_Kong' => '(GMT+0800) Hong Kong SAR China Time',
			'Asia/Makassar' => '(GMT+0800) Indonesia (Makassar) Time',
			'Asia/Macau' => '(GMT+0800) Macau SAR China Time',
			'Asia/Kuala_Lumpur' => '(GMT+0800) Malaysia (Kuala Lumpur) Time',
			'Asia/Choibalsan' => '(GMT+0800) Mongolia (Choibalsan) Time',
			'Asia/Ulaanbaatar' => '(GMT+0800) Mongolia (Ulaanbaatar) Time',
			'Asia/Manila' => '(GMT+0800) Philippines Time',
			'Asia/Krasnoyarsk' => '(GMT+0800) Russia (Krasnoyarsk) Time',
			'Asia/Singapore' => '(GMT+0800) Singapore Time',
			'Asia/Taipei' => '(GMT+0800) Taiwan Time',
			'Asia/Dili' => '(GMT+0900) East Timor Time',
			'Asia/Jayapura' => '(GMT+0900) Indonesia (Jayapura) Time',
			'Asia/Tokyo' => '(GMT+0900) Japan Time',
			'Asia/Pyongyang' => '(GMT+0900) North Korea Time',
			'Pacific/Palau' => '(GMT+0900) Palau Time',
			'Asia/Irkutsk' => '(GMT+0900) Russia (Irkutsk) Time',
			'Asia/Seoul' => '(GMT+0900) South Korea Time',
			'Australia/Darwin' => '(GMT+0930) Australia (Darwin) Time',
			'Antarctica/DumontDUrville' => '(GMT+1000) Antarctica (Dumont d’Urville) Time',
			'Australia/Brisbane' => '(GMT+1000) Australia (Brisbane) Time',
			'Pacific/Guam' => '(GMT+1000) Guam Time',
			'Pacific/Truk' => '(GMT+1000) Micronesia (Truk) Time',
			'Pacific/Saipan' => '(GMT+1000) Northern Mariana Islands Time',
			'Pacific/Port_Moresby' => '(GMT+1000) Papua New Guinea Time',
			'Asia/Yakutsk' => '(GMT+1000) Russia (Yakutsk) Time',
			'Australia/Adelaide' => '(GMT+1030) Australia (Adelaide) Time',
			'Australia/Hobart' => '(GMT+1100) Australia (Hobart) Time',
			'Australia/Melbourne' => '(GMT+1100) Australia (Melbourne) Time',
			'Australia/Sydney' => '(GMT+1100) Australia (Sydney) Time',
			'Pacific/Kosrae' => '(GMT+1100) Micronesia (Kosrae) Time',
			'Pacific/Ponape' => '(GMT+1100) Micronesia (Ponape) Time',
			'Pacific/Noumea' => '(GMT+1100) New Caledonia Time',
			'Asia/Vladivostok' => '(GMT+1100) Russia (Vladivostok) Time',
			'Pacific/Guadalcanal' => '(GMT+1100) Solomon Islands Time',
			'Pacific/Efate' => '(GMT+1100) Vanuatu Time',
			'Pacific/Norfolk' => '(GMT+1130) Norfolk Island Time',
			'Pacific/Tarawa' => '(GMT+1200) Kiribati (Tarawa) Time',
			'Pacific/Kwajalein' => '(GMT+1200) Marshall Islands (Kwajalein) Time',
			'Pacific/Majuro' => '(GMT+1200) Marshall Islands (Majuro) Time',
			'Pacific/Nauru' => '(GMT+1200) Nauru Time',
			'Asia/Kamchatka' => '(GMT+1200) Russia (Kamchatka) Time',
			'Asia/Magadan' => '(GMT+1200) Russia (Magadan) Time',
			'Pacific/Funafuti' => '(GMT+1200) Tuvalu Time',
			'Pacific/Wake' => '(GMT+1200) United States Minor Outlying Islands (Wake) Time',
			'Pacific/Wallis' => '(GMT+1200) Wallis and Futuna Time',
			'Pacific/Fiji' => '(GMT+1300) Fiji Time',
			'Pacific/Enderbury' => '(GMT+1300) Kiribati (Enderbury) Time',
			'Pacific/Auckland' => '(GMT+1300) New Zealand (Auckland) Time',
			'Pacific/Fakaofo' => '(GMT+1300) Tokelau Time',
			'Pacific/Tongatapu' => '(GMT+1300) Tonga Time',
			'Pacific/Kiritimati' => '(GMT+1400) Kiribati (Kiritimati) Time',
			'Pacific/Apia' => '(GMT+1400) Samoa Time',
			);
			$toReturn .= "<optgroup label='US Timezones'>";
			foreach($usOptions as $key => $value) {
				  $selectedOption = '';
				  if($key == $selected) {
						$selectedOption = ' selected="selected"';
				  }
				  $toReturn .= '
				  <option value="'.$key.'"'.$selectedOption.'>'.$value.'</option>';
			}
			$toReturn .= "</optgroup>";

			$toReturn .= "<optgroup label='All Other Timezones'>";
			foreach($options as $key => $value) {
				  $selectedOption = '';
				  if($key == $selected) {
						$selectedOption = ' selected="selected"';
				  }
				  $toReturn .= '
				  <option value="'.$key.'"'.$selectedOption.'>'.$value.'</option>';
			}
			$toReturn .= "</optgroup>";
		$toReturn .= '</select>';
		return $toReturn;
	}

/**
 * Displays the long name/description of a timezone based on it's index (ie '+5.0')
 * @param  [type] $index [description]
 * @return [type]        [description]
 */
	  public function display($index) {
			return $this->output($this->timezones[$index]);
	  }



/**
 * 
 * @return [type] [description]
 */
	  public function timeOptions12() {
			return $timeOptions = array(
				  '00:00:00' => '12:00 AM',
				  '00:30:00' => '12:30 AM',
				  '01:00:00' => '1:00 AM',
				  '01:30:00' => '1:30 AM',
				  '02:00:00' => '2:00 AM',
				  '02:30:00' => '2:30 AM',
				  '03:00:00' => '3:00 AM',
				  '03:30:00' => '3:30 AM',
				  '04:00:00' => '4:00 AM',
				  '04:30:00' => '4:30 AM',
				  '05:00:00' => '5:00 AM',
				  '05:30:00' => '5:30 AM',
				  '06:00:00' => '6:00 AM',
				  '06:30:00' => '6:30 AM',
				  '07:00:00' => '7:00 AM',
				  '07:30:00' => '7:30 AM',
				  '08:00:00' => '8:00 AM',
				  '08:30:00' => '8:30 AM',
				  '09:00:00' => '9:00 AM',
				  '09:30:00' => '9:30 AM',
				  '10:00:00' => '10:00 AM',
				  '10:30:00' => '10:30 AM',
				  '11:00:00' => '11:00 AM',
				  '11:30:00' => '11:30 AM',
				  '12:00:00' => '12:00 PM',
				  '12:30:00' => '12:30 PM',
				  '13:00:00' => '1:00 PM',
				  '13:30:00' => '1:30 PM',
				  '14:00:00' => '2:00 PM',
				  '14:30:00' => '2:30 PM',
				  '15:00:00' => '3:00 PM',
				  '15:30:00' => '3:30 PM',
				  '16:00:00' => '4:00 PM',
				  '16:30:00' => '4:30 PM',
				  '17:00:00' => '5:00 PM',
				  '17:30:00' => '5:30 PM',
				  '18:00:00' => '6:00 PM',
				  '18:30:00' => '6:30 PM',
				  '19:00:00' => '7:00 PM',
				  '19:30:00' => '7:30 PM',
				  '20:00:00' => '8:00 PM',
				  '20:30:00' => '8:30 PM',
				  '21:00:00' => '9:00 PM',
				  '21:30:00' => '9:30 PM',
				  '22:00:00' => '10:00 PM',
				  '23:30:00' => '10:30 PM',
				  '23:00:00' => '11:00 PM',
				  '23:30:00' => '11:30 PM',
			);
	  }

/**
 * [timeOptions24 description]
 * @return [type] [description]
 */
	  public function timeOptions24() {
			return $timeOptions = array(
				  '00:00:00' => '12:00',
				  '00:30:00' => '12:30',
				  '01:00:00' => '1:00',
				  '01:30:00' => '1:30',
				  '02:00:00' => '2:00',
				  '02:30:00' => '2:30',
				  '03:00:00' => '3:00',
				  '03:30:00' => '3:30',
				  '04:00:00' => '4:00',
				  '04:30:00' => '4:30',
				  '05:00:00' => '5:00',
				  '05:30:00' => '5:30',
				  '06:00:00' => '6:00',
				  '06:30:00' => '6:30',
				  '07:00:00' => '7:00',
				  '07:30:00' => '7:30',
				  '08:00:00' => '8:00',
				  '08:30:00' => '8:30',
				  '09:00:00' => '9:00',
				  '09:30:00' => '9:30',
				  '10:00:00' => '10:00',
				  '10:30:00' => '10:30',
				  '11:00:00' => '11:00',
				  '11:30:00' => '11:30',
				  '12:00:00' => '12:00',
				  '12:30:00' => '12:30',
				  '13:00:00' => '13:00',
				  '13:30:00' => '13:30',
				  '14:00:00' => '14:00',
				  '14:30:00' => '14:30',
				  '15:00:00' => '15:00',
				  '15:30:00' => '15:30',
				  '16:00:00' => '16:00',
				  '16:30:00' => '16:30',
				  '17:00:00' => '17:00',
				  '17:30:00' => '17:30',
				  '18:00:00' => '18:00',
				  '18:30:00' => '18:30',
				  '19:00:00' => '19:00',
				  '19:30:00' => '19:30',
				  '20:00:00' => '20:00',
				  '20:30:00' => '20:30',
				  '21:00:00' => '21:00',
				  '21:30:00' => '21:30',
				  '22:00:00' => '22:00',
				  '23:00:00' => '22:30',
				  '23:30:00' => '23:30',
			);
	  }
}
?>