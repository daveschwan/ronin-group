<?php
class MetaDataHelper extends AppHelper {

	//finds (and returns) the first non-empty value after cleaning and trimming it
	public function tagData($opts=array()) {

		//sets defaults
		$defaults = array(
			'tag' => '',
			'possibles' => array()
		);
		$data = '';
		$params = array_merge($defaults, $opts);

		//finds and returns cleaned & trimmed first non-empty
		foreach($params['possibles'] as $data) {
			if(!empty($data)) {
				$data = $this->clean($data);
				$data = $this->trim($this->clean($data), $params['tag']);
				break;
			}
		}

		return $data;
	}

	public function clean($data=null) {
		$data = strip_tags($data);
		return $data;
	}

	public function trim($data=null, $type=null) {
		$maxChars = 400;
		switch($type) {
			case "title": $maxChars = 100; break;
			case "description": $maxChars = 400; break;
			case "keywords": $maxChars = 100; break;
			case "author": $maxChars = 100; break;
			case "robots": $maxChars = 40; break;
		}
		return mb_substr($data, 0, $maxChars);
	}

}