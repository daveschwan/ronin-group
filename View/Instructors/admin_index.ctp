
<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-calendar fa-fw "></i>
			Instructors
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading" class="drk">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2>Instructor List</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th><?php echo $this->Paginator->sort('Instructor.name', 'Name'); ?></th>
								<th>Profile</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($instructors as $instructor) {
								?>
								<tr>
									<td><?php echo $this->Html->link($instructor['Instructor']['name'], array('controller'=>'instructors', 'action'=>'edit', $instructor['Instructor']['id'])); ?></td>
									<td><?php echo $this->Text->truncate(strip_tags($instructor['Instructor']['profile']), 60); ?></td>
								</tr>
							<?php
							}
							?>
						</tbody>
					</table>
					<?php
					echo $this->Html->link('Add an Instructor', array('controller'=>'instructors', 'action'=>'add'), array('class'=>'btn-bottom-add btn btn-primary'));
					echo $this->Element('admin/paging');
					?>

				</div>

			</div>

		</div>

	</article>
</div>