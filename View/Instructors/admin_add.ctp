
<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-calendar fa-fw "></i>
			<?php echo $this->Html->link('Instructors', array('controller'=>'instructors', 'action'=>'index')); ?>
			<span>&nbsp;&nbsp; &gt; &nbsp;&nbsp;</span>
			Add
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2>Instructor Info</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<?php
					echo $this->Form->create('Instructor', array('inputDefaults'=>array('label'=>false,'div'=>false)));
						?>
						<fieldset>

							<div class="form-group">
								<label>Name</label>
								<?php echo $this->Form->input('Instructor.name', array('class'=>'form-control input-lg')); ?>
								<p class="note">The full name of the instructor.  Example: John Doe</p>
							</div>

							<div class="form-group">
								<label>Email</label>
								<?php echo $this->Form->input('Instructor.email', array('class'=>'form-control input-lg')); ?>
								<p class="note">The email address of the instructor.  This will be displayed in their details.  Example: johndoe@gmail.com</p>
							</div>

							<div class="form-group">
								<label>Phone</label>
								<?php echo $this->Form->input('Instructor.phone', array('class'=>'form-control input-lg')); ?>
								<p class="note">Phone number of the instructor.  This will be displayed in their details.  Example 888-555-1234</p>
							</div>

							<div class="form-group">
								<label>Website URL</label>
								<?php echo $this->Form->input('Instructor.url', array('class'=>'form-control input-lg')); ?>
								<p class="note">The instructor's website.  This will be displayed in their details.  Example www.johndoe.com</p>
							</div>

							<div class="form-group">
								<label>Profile</label>
								<?php echo $this->Form->input('Instructor.profile', array('class'=>'rte form-control input-lg tall')); ?>
								<p class="note">Short profile/description of this instructor.</p>
							</div>

						</fieldset>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->Html->link('Cancel', array('controller'=>'instructors', 'action'=>'index'), array('class'=>'btn btn-default')); ?>
									<?php echo $this->Form->submit('Add Instructor', array('class'=>'btn btn-primary', 'div'=>false)); ?>
								</div>
							</div>
						</div>

					<?php
					echo $this->Form->end();
					?>

				</div>

			</div>

		</div>

	</article>
</div>