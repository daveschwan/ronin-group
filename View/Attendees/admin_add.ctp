<?php
$this->Html->script(
	array(
		'admin/attendees',
	),
	array('inline'=>false)
);
?>

<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-calendar fa-fw "></i>
			<?php echo $this->Html->link($this->Text->truncate($courseInstance['Course']['name'], 20), array('controller'=>'courses', 'action'=>'view', $courseInstance['Course']['id'])); ?>
			<span>&nbsp;&nbsp; &gt; &nbsp;&nbsp;</span>
			<?php echo $this->Html->link($this->Text->truncate($courseInstance['CourseInstance']['location'], 20), array('controller'=>'course_instances', 'action'=>'view', $courseInstance['CourseInstance']['id'])); ?>
			<span>&nbsp;&nbsp; &gt; &nbsp;&nbsp;</span>
			Add Attendee
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2>Attendee Info</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<?php
					echo $this->Form->create('Attendee', array('inputDefaults'=>array('label'=>false,'div'=>false)));
						echo $this->Form->input('course_instance_id', array('type'=>'hidden', 'value'=>$courseInstance['CourseInstance']['id']));
						echo $this->Form->input('create_user', array('type'=>'hidden', 'value'=>false));
						?>
						<fieldset id="existingUserFields">

							<div class="form-group col-md-12">
								<label>Existing User</label><br>
								<?php echo $this->Form->input('user_id', array('empty'=>true, 'class'=>'form-control input-lg')); ?>
								<p class="note">Select an Existing User to add as an attendee or <?php echo $this->Html->link('Create a New User', 'javascript:showNewUserFields();'); ?>.</p>
							</div>

						</fieldset>

						<fieldset id="newUserFields" style="display:none;">

							<div class="form-group col-md-12">
								<label>Name</label>
								<?php echo $this->Form->input('User.name', array('class'=>'form-control input-lg')); ?>
								<p class="note">The full name (not 'username') of the user.  Example: John Doe"</p>
							</div>

							<div class="form-group col-md-12">
								<label>Email Address</label>
								<?php echo $this->Form->input('User.email', array('class'=>'form-control input-lg')); ?>
								<p class="note">The full email address of the user.  IMPORTANT: This will be used for their log-in.</p>
							</div>

							<div class="row col-md-12">
								<div class="form-group col-md-6">
									<label>Password</label>
									<?php echo $this->Form->input('User.password', array('class'=>'form-control input-lg')); ?>
									<p class="note">Must be at least 8 characters in length.</p>
								</div>

								<div class="form-group col-md-6">
									<label>Verify Password</label>
									<?php echo $this->Form->input('User.re_password', array('type'=>'password', 'class'=>'form-control input-lg', 'disabled')); ?>
									<p class="note">Must exactly match the "Password".</p>
								</div>
							</div>

							<div class="col-md-12">
								<?php
								echo $this->Html->link('Select an existing User', 'javascript:showExistingUserFields()', array('id'=>'existingUserButton'));
								?>
							</div>

						</fieldset>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->Html->link('Cancel', array('controller'=>'course_instances', 'action'=>'view', $courseInstance['CourseInstance']['id']), array('class'=>'btn btn-default')); ?>
									<?php echo $this->Form->submit('Save', array('class'=>'btn btn-primary', 'div'=>false)); ?>
								</div>
							</div>
						</div>

					<?php
					echo $this->Form->end();
					?>

				</div>

			</div>

		</div>

	</article>
</div>


<script>
	$(document).ready(function() {
		initializePage();
	});
</script>