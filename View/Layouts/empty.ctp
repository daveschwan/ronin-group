<!DOCTYPE html>
<?php
$this->Html->loadConfig('html5_tags');
?>
<html lang="en">
<head>
	<?php
	echo $this->Html->charset();
	$title = '';
	?>
	<title><?php echo $title ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

	<?php
	echo $this->Html->meta('icon');

	echo $this->fetch('meta');

	// STYLES *****************
	echo $this->fetch('css');

	// SCRIPTS ****************
	echo $this->fetch('script');

	// Google Site Verification
	if(Configure::check('Event.Service.google_site_verification'))  echo '<meta name="google-site-verification" content="' . Configure::read('Event.Service.google_site_verification') . '" />
	';

	// Bing Site Verification
	if(Configure::check('Event.Service.bing_site_verification')) echo '<meta name="msvalidate.01" content="' . Configure::read('Event.Service.bing_site_verification') . '" />
	';
	?>
</head>
<body>
	<?php
	echo $this->Session->flash();
	echo $this->Session->flash('auth');
	echo $this->fetch('content');
	?>
</body>