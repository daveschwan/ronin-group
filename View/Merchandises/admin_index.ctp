
<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-group fa-fw "></i>
			Merchandise
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading" class="drk">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2>Merchandise List</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th><?php echo $this->Paginator->sort('Merchandise.name', 'Name'); ?></th>
								<th><?php echo $this->Paginator->sort('Merchandise.description', 'Description'); ?></th>
								<th class="center"><?php echo $this->Paginator->sort('Merchandise.active', 'Active'); ?></th>
								<th class="center">Delete</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($merchandises as $merchandise) {
								?>
								<tr>
									<td><?php echo $this->Html->link($merchandise['Merchandise']['name'], array('controller'=>'merchandises', 'action'=>'edit', $merchandise['Merchandise']['id'])); ?></td>
									<td><?php echo $this->Text->truncate(strip_tags($merchandise['Merchandise']['description']), 40); ?></td>
									<td class="center">
										<?php
										$icon = ($merchandise['Merchandise']['active']) ? 'fa-check' : 'fa-minus';
										echo '<i class="fa ' . $icon . ' fa-lg"></i>'
										?>
									</td>
									<td class="center">
										<?php
										echo $this->Html->link('<i class="fa fa-times fa-lg"></i>', array('controller'=>'merchandises', 'action'=>'delete', $merchandise['Merchandise']['id']), array('escape'=>false), 'Are you sure you want to delete the merchandise: "' . $merchandise['Merchandise']['name'] . '"?');
										?>
									</td>
								</tr>
							<?php
							}
							?>
						</tbody>
					</table>

				</div>

			</div>

		</div>

	</article>
</div>