<?php
// echo $this->Html->script(
// 	array(
// 		'admin/merchandises'
// 	),
// 	array(
// 		'inline'=>false
// 	)
// );
?>

<div class="row">
	<div class="col-md-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-calendar fa-fw "></i>
			<?php echo $this->Html->link('Merchandise', array('controller'=>'merchandises', 'action'=>'index')); ?>
			<span>&nbsp;&nbsp; &gt; &nbsp;&nbsp;</span>Edit
		</h1>
	</div>
</div>


<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2>General Merchandise Info</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<?php
					echo $this->Form->create('Merchandise', array('inputDefaults'=>array('label'=>false,'div'=>false)));
						echo $this->Form->input('Merchandise.id');
						?>
						<fieldset>

							<div class="form-group">
								<label>Name</label>
								<?php echo $this->Form->input('Merchandise.name', array('class'=>'form-control input-lg')); ?>
								<p class="note">The full name of the merchandise.  Example: Advanced Rifle Training"</p>
							</div>

							<div class="form-group">
								<label>Description</label>
								<?php echo $this->Form->input('Merchandise.description', array('class'=>'rte form-control input-lg tall')); ?>
								<p class="note">General description of the merchandise.</p>
							</div>

							<div class="form-group">
								<div class="checkbox">
									<label>
										<?php echo $this->Form->input('Merchandise.active', array('class'=>'checkbox style-0')); ?>
										<span>Active (Visible)</span>
									</label>
								</div>
								<p class="note">Whether or not this merchandise is visible to users.</p>
							</div>

						</fieldset>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->Html->link('Cancel', array('controller'=>'merchandises', 'action'=>'index'), array('class'=>'btn btn-default')); ?>
									<?php echo $this->Form->submit('Save Changes to Merchandise', array('class'=>'btn btn-primary', 'div'=>false)); ?>
								</div>
							</div>
						</div>

					<?php
					echo $this->Form->end();
					?>

				</div>

			</div>

		</div>

	</article>
</div>






<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

		<div class="jarviswidget">

			<header role="heading" class="drk">
				<span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
				<h2>Options Categories</h2>
			</header>

			<div role="content">

				<div class="widget-body">

					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th>Name</th>
								<th class="center">Delete</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($merchandiseOptionCategories as $merchandiseOptionCategory) {
								?>
								<tr>
									<td><?php echo $this->Html->link($merchandiseOptionCategory['MerchandiseOptionCategory']['name'], array('controller'=>'merchandises', 'action'=>'edit', $merchandise['Merchandise']['id'])); ?></td>
									<td class="center">
										<?php
										echo $this->Html->link('<i class="fa fa-times fa-lg"></i>', array('controller'=>'merchandises', 'action'=>'delete', $merchandise['Merchandise']['id']), array('escape'=>false), 'Are you sure you want to delete the merchandise: "' . $merchandise['Merchandise']['name'] . '"?');
										?>
									</td>
								</tr>
								<?php
							}
							?>
							<tr>
								<td colspan="2">
									<?php
									echo $this->Form->create('MerchandiseOptionCategory', array(
										'inputDefaults' => array('div'=>false, 'label'=>false),
										'onsubmit' => 'addMerchandiseOptionCategory(); return false;'
									));
									$this->Form->unlockField('MerchandiseOptionCategory.name');
									$this->Form->unlockField('MerchandiseOptionCategory.merchandise_id');
									echo $this->Form->input('MerchandiseOptionCategory.merchandise_id', array('class'=>'inline-table', 'type'=>'hidden', 'value'=>$merchandise['Merchandise']['id']));
									echo $this->Form->input('MerchandiseOptionCategory.name', array('class'=>'inline-table'));
									echo $this->Form->submit('Add a new Option Category', array('div'=>false));
									?>
								</td>
							</tr>
						</tbody>
					</table>

				</div>

			</div>

		</div>

	</article>
</div>



<div class="row">

	<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
		<?php echo $this->Html->link('Delete this Merchandise', array('controller'=>'merchandises', 'action'=>'delete', $merchandise['Merchandise']['id']), array(), "Are you sure you want to delete this merchandise?"); ?>
	</article>

</div>



<script>
	function addMerchandiseOptionCategory() {
		var category = $('#MerchandiseOptionCategoryName').val();

		$.ajax({
			type: "POST",
			url: "<?php echo $this->Html->url(array('controller'=>'merchandise_option_categories', 'action'=>'ajax_add', 'admin'=>true)); ?>",
			data: $('#MerchandiseOptionCategoryAdminEditForm').serialize(),
			success: function(data) {
				returnedData = jQuery.parseJSON(data);
				// if(returnedData['status'] == 'success') {
				// 	$('#previewSent_success').show();
				// 	$('#previewSent_fail').hide();
				// } else {
				// 	$('#previewSent_success').hide();
				// 	$('#previewSent_fail').show();
				// }
				// $('#sendPreviewButton').prop('disabled', false);
				// $('#MessagePreviewEmail').prop('disabled', false);
				// $('#sendPreviewArea').fadeIn();
			},
			error: function(errorMessage) {
				// $('#sendPreviewButton').prop('disabled', false);
				// $('#MessagePreviewEmail').prop('disabled', false);
				// $('#sendPreviewArea').fadeIn();
				// $('#previewSent_success').hide();
				// $('#previewSent_fail').show();
			}
		});
		return false;
	}
</script>