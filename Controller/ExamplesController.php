<?php
class ExamplesController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();
	}
	public function admin_blank_() { }
	public function admin_bootstrap_forms() { }
	public function admin_buttons() { }
	public function admin_calendar() { }
	public function admin_ckeditor() { }
	public function admin_datatables() { }
	public function admin_dropzone() { }
	public function admin_dummy() { }
	public function admin_email_template() { }
	public function admin_error404() { }
	public function admin_error500() { }
	public function admin_fa() { }
	public function admin_flot() { }
	public function admin_forgotpassword() { }
	public function admin_form_elements() { }
	public function admin_form_templates() { }
	public function admin_forum() { }
	public function admin_forum_post() { }
	public function admin_forum_topic() { }
	public function admin_gallery() { }
	public function admin_general_elements() { }
	public function admin_glyph() { }
	public function admin_gmap_xml() { }
	public function admin_grid() { }
	public function admin_inbox() { }
	public function admin_index() { }
	public function admin_inline_charts() { }
	public function admin_invoice() { }
	public function admin_jqui() { }
	public function admin_lock() { }
	public function admin_login() { }
	public function admin_morris() { }
	public function admin_nestable_list() { }
	public function admin_other_editors() { }
	public function admin_plugins() { }
	public function admin_pricing_table() { }
	public function admin_profile() { }
	public function admin_register() { }
	public function admin_search() { }
	public function admin_table() { }
	public function admin_timeline() { }
	public function admin_treeview() { }
	public function admin_typography() { }
	public function admin_upload() { }
	public function admin_validation() { }
	public function admin_widgets() { }
	public function admin_wizard() { }
}