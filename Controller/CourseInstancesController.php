<?php
class CourseInstancesController extends AppController {

	public function beforeFilter() {
		$this->Security->unlockedActions = array('admin_add', 'admin_edit');
		parent::beforeFilter();
	}




// adds a new course instance
	public function admin_add($courseId = null) {
		if(empty($courseId)) {
			$this->redirect($this->referer());
		}
		if($this->request->is('post')) {
			$courseInstance = $this->CourseInstance->add($this->request->data);
			if($courseInstance) {
				$this->Session->setFlash('The Course Instance has been created.', 'admin/notifications', array('type'=>'success'));
				$this->redirect(array('controller'=>'courses', 'action'=>'view', $courseId));
			} else {
				$this->Session->setFlash('The Course Instance could not be created.', 'admin/notifications', array('type'=>'error'));
			}
		}
		$course = $this->CourseInstance->Course->getCourse(array('id'=>$courseId, 'activeOnly' => false));

		$this->set(compact('course'));

	}



// views an existing course instance
	public function admin_view($id = null) {
		if(!$this->CourseInstance->exists($id)) {
			throw new NotFoundException('Could not find Course Instance.');
		}
		$courseInstance = $this->CourseInstance->getCourseInstance($id, array('activeOnly' => false));
		$course = array('Course' => $courseInstance['Course']);

		// gets the attendees for this course instance
		$opts = array(
			'paginate'   => true,
			'limit'      => 30,
		);
		$this->Paginator->settings = $this->CourseInstance->Attendee->getAttendees($id, $opts);
		$this->Paginator->settings['paramType'] = 'querystring';
		$this->Paginator->settings['order'] = array('Attendee.name'=>'asc');
		$attendees = $this->paginate('Attendee');

		// $attendees = $this->CourseInstance->Attendee->getAttendees($id);
		$this->set(compact('course', 'courseInstance', 'attendees'));
	}



// edits an existing course instance
	public function admin_edit($id = null) {
		if(!$this->CourseInstance->exists($id)) {
			throw new NotFoundException('Could not find Course Instance.');
		}
		if($this->request->is('put')) {
			if($this->CourseInstance->edit($this->request->data)) {
				$this->Session->setFlash('Changes to the Course Instance have been saved.', 'admin/notifications', array('type'=>'success'));
			} else {
				$this->Session->setFlash('Changes to the Course Instance could not be saved.', 'admin/notifications', array('type'=>'error'));
			}
		}
		$courseInstance = $this->CourseInstance->getCourseInstance($id, array('activeOnly' => false));
		$this->request->data = $courseInstance;

		$course = array('Course' => $courseInstance['Course']);

		$this->set(compact('course', 'courseInstance'));
	}


/**
 * Deletes a course instance
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
	public function admin_delete($id = null) {
		$courseId = $this->CourseInstance->courseId($id);
		$this->CourseInstance->deleteCourseInstance($id, 'deleted');
		$this->Session->setFlash(__('The course instance has been deleted.'), 'admin/notifications', array('type'=>'success'));
		$this->redirect(array('controller'=>'courses', 'action'=>'view', $courseId));
	}

}