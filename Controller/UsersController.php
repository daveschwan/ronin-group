<?php
class UsersController extends AppController {


	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(array(
			'register',
			'login',
			'logout',
			'admin_login',
		));
		// $this->Auth->allow();
		$this->Security->unlockedActions = array('admin_edit'); // not idea, but can't get it to work any other way
	}

/**
 * Allows a new User to register for an account
 * @return [type] [description]
 */
	public function register() {
		$this->layout = 'empty';
		if($this->request->is('post')) {
			$user = $this->User->register($this->request->data);
			if($user) {
				if($this->Auth->login()) {
					$this->Session->setFlash('Your account has been created!.', 'notifications', array('type'=>'success'));
					$this->redirect(array('controller'=>'users', 'action'=>'account'));
				} else {
					$this->Session->setFlash('Your account has been created!  Please log-in to continue.', 'notifications', array('type'=>'success'));
					$this->redirect(array('controller'=>'users', 'action'=>'login', 'admin'=>false));
				}
			} else {
				$this->Session->setFlash('Your accound could not be created.', 'notifications', array('type'=>'error'));
			}
		}
	}


/**
 * Allows a returning User to log in to their account
 * @return [type] [description]
 */
	public function login() {
		$this->Auth->logout();
	    if ($this->request->is('post')) {
	        if($this->Auth->login()) {
	        	$this->User->id = $this->Auth->user('id');
	        	$this->User->saveField('last_login', date('Y-m-d'));
	            $this->Session->setFlash(__('You have successully logged in!'), 'notifications', array('type'=>'success'));
	            $this->redirect($this->Auth->redirect());
	        } else {
	            $this->Session->setFlash(__('Invalid email address or password, try again.'), 'notifications', array('type'=>'error'));
	        }
	    }
	}



/**
 * Logs a user out
 * @return [type] [description]
 */
	public function logout() {
		$this->redirect($this->Auth->logout());
	}





/**
 * Admin Login
 * @return [type] [description]
 */
	public function admin_login() {
		$this->Auth->logout();
		$this->layout = 'admin_login';
	    if ($this->request->is('post')) {
	        if($this->Auth->login()) {
	        	$this->User->id = $this->Auth->user('id');
	        	$this->User->saveField('last_login', date('Y-m-d'));
	            $this->Session->setFlash(__('You have successully logged in!'), 'notifications', array('type'=>'success'));
	            $this->redirect($this->Auth->redirect());
	        } else {
	            $this->Session->setFlash(__('Invalid email address or password, try again.'), 'notifications', array('type'=>'error'));
	        }
	    }
	}


/**
 * ADMIN ONLY:
 * List all the users
 */
	public function admin_index() {
		$opts = array(
			'activeOnly' => false,
			'paginate'   => true,
			'limit'      => 30,
		);
		$this->Paginator->settings = $this->User->getUsers($opts);
		$this->Paginator->settings['paramType'] = 'querystring';
		$this->Paginator->settings['order'] = array('User.name'=>'asc');
		$users = $this->paginate('User');
		$this->set(compact('users'));
	}



/**
 * ADMIN ONLY:
 * Creates a user
 */
	public function admin_add() {
		if($this->request->is('post')) {
			$user = $this->User->add($this->request->data);
			if($user) {
				$this->Session->setFlash('The User has been created.', 'admin/notifications', array('type'=>'success'));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash('The User could not be created.', 'admin/notifications', array('type'=>'error'));
			}
		}
		$roles = $this->User->Role->getRoles(array('findType' => 'list'));
		$this->set(compact('roles'));
	}



/**
 * ADMIN ONLY:
 * Edit a user's info and permissions
 * id of the item to edit
 * @var [type]
 */
	public function admin_edit($id = null) {
		if(!$this->User->exists($id)) {
			throw new NotFoundException('Could not find User.');
		}
		if($this->request->is('put')) {
			if($this->User->edit($this->request->data)) {
				$this->Session->setFlash('The User has been saved.', 'admin/notifications', array('type'=>'success'));
			} else {
				$this->Session->setFlash('The User could not be saved.', 'admin/notifications', array('type'=>'error'));
			}
		}
		$user = $this->User->getUser(array('id'=>$id));
		$this->request->data = $user;

		$this->set(compact('user'));
	}



/**
 * Deletes a specified user
 * id of the item to delete
 * @var [type]
 */
	public function admin_delete($id = null) {

		// if no id, redirect back
		if(empty($id)) {
            $this->Session->setFlash(__('There was a problem deleting that user.'), 'admin/notifications', array('type'=>'fail'));
			$this->redirect($this->referer());
		}

		$this->layout = false;
		$currentUser = CakeSession::read("Auth.User");

		// make sure they have permission to delete a user
		if($currentUser['Role']['name'] != 'Administrator') {
        	$this->Session->setFlash(__('You do not have permission to delete users.'), 'admin/notifications', array('type'=>'error'));
        	$this->redirect($this->referer());
		}

		// gets the to-be-deleted user
		$userToBeDeleted = $this->User->findById($id);

		// attempt to delete the user
		if($this->User->deleteUser($id)) {
        	$this->Session->setFlash(__('The user has been deleted.'), 'admin/notifications', array('type'=>'success'));

        	// if they deleted themselves, then log them out
        	if($currentUser['id'] == $id) {
        		$this->redirect($this->Auth->logout());
        	}
        } else {
        	$this->Session->setFlash(__('The user could not be deleted.'), 'admin/notifications', array('type'=>'error'));
        }
		$this->redirect($this->referer());
	}






}