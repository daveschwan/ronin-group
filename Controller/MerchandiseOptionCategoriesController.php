<?php
class MerchandiseOptionCategoriesController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
		// $this->Security->unlockedActions(array('admin_ajax_add'));
	}


	public function admin_ajax_add() {
		// $this->Auth->allow('Merchandise');
		$this->layout = false;
		$saved = $this->MerchandiseOptionCategory->add($this->request->data);

		$data = array('status'=>'fail');
		if($saved) {
			$data['status'] = 'success';
		}
		$this->set('data', $data);
		$this->render('/Ajax/json');
	}

}