<?php
class InstructorsController extends AppController {



	public function beforeFilter() {
		$this->Security->unlockedActions = array('admin_add', 'admin_edit');
		parent::beforeFilter();
	}



/**
 * List of all the instructors
 * @return [type] [description]
 */
	public function admin_index() {
		$opts = array(
			'paginate'   => true,
			'limit'      => 30,
		);
		$this->Paginator->settings = $this->Instructor->getInstructors($opts);
		$this->Paginator->settings['paramType'] = 'querystring';
		$this->Paginator->settings['order'] = array('Instructor.name'=>'asc');
		$instructors = $this->paginate('Instructor');
		$this->set(compact('instructors'));
	}



// adds a new instructor
	public function admin_add() {
		if($this->request->is('post')) {
			$instructor = $this->Instructor->add($this->request->data);
			if($instructor) {
				$this->Session->setFlash('The Instructor has been created.', 'admin/notifications', array('type'=>'success'));
				$this->redirect(array('controller'=>'instructors', 'action'=>'index'));
			} else {
				$this->Session->setFlash('The Instructor could not be created.', 'admin/notifications', array('type'=>'error'));
			}
		}
	}



// edits an existing instructor
	public function admin_edit($id = null) {
		if(!$this->Instructor->exists($id)) {
			throw new NotFoundException('Could not find Instructor.');
		}
		if($this->request->is('put')) {
			if($this->Instructor->edit($this->request->data)) {
				$this->Session->setFlash('Changes to the Instructor have been saved.', 'admin/notifications', array('type'=>'success'));
			} else {
				$this->Session->setFlash('Changes to the Instructor could not be saved.', 'admin/notifications', array('type'=>'error'));
			}
		}
		$instructor = $this->Instructor->getInstructor(array('id'=>$id, 'activeOnly' => false));
		$this->request->data = $instructor;

		$this->set(compact('instructor'));
	}


/**
 * Deletes a instructor
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
	public function admin_delete($id = null) {
		$this->Instructor->deleteInstructor($id, 'deleted');
		$this->Session->setFlash(__('The instructor has been deleted.'), 'admin/notifications', array('type'=>'success'));
		$this->redirect(array('controller'=>'instructors', 'action'=>'index'));
	}

}