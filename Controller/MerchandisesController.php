<?php
class MerchandisesController extends AppController {


/**
 * [beforeFilter description]
 * @return [type] [description]
 */
	public function beforeFilter() {
		parent::beforeFilter();
		// $this->Security->unlockedActions(array('admin_edit'));
	}


/**
 * [admin_index description]
 * @return [type] [description]
 */
	public function admin_index() {
		$opts = array(
			'activeOnly' => false,
			'paginate'   => true,
			'limit'      => 30,
		);
		$this->Paginator->settings = $this->Merchandise->getMerchandises($opts);
		$this->Paginator->settings['paramType'] = 'querystring';
		$this->Paginator->settings['order'] = array('Merchandise.name'=>'asc');
		$merchandises = $this->paginate('Merchandise');
		$this->set(compact('merchandises'));
	}




/**
 * [admin_add description]
 * @return [type] [description]
 */
	public function admin_add() {
		if($this->request->is('post')) {
			$merchandise = $this->Merchandise->add($this->request->data);
			if($merchandise) {
				$this->Session->setFlash('The Merchandise has been created.', 'admin/notifications', array('type'=>'success'));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash('The Merchandise could not be created.', 'admin/notifications', array('type'=>'error'));
			}
		}

	}



/**
 * Edit an existing Merchandise
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
	public function admin_edit($id = null) {
		if(!$this->Merchandise->exists($id)) {
			throw new NotFoundException('Could not find Merchandise.');
		}
		if($this->request->is('put')) {
			if($this->Merchandise->edit($this->request->data)) {
				$this->Session->setFlash('Changes to the merchandise have been saved.', 'admin/notifications', array('type'=>'success'));
			} else {
				$this->Session->setFlash('Changes to the merchandise could not be saved.', 'admin/notifications', array('type'=>'error'));
			}
		}
		$merchandise = $this->Merchandise->getMerchandise(array('id'=>$id, 'activeOnly' => false));
		$this->request->data = $merchandise;

		$merchandiseCategories = $this->Merchandise->MerchandiseCategory->find('list');
		$merchandiseOptionCategories = $this->Merchandise->MerchandiseOptionCategory->getMerchandiseOptionCategories($id);

		$this->set(compact(
			'merchandise',
			'merchandiseCategories',
			'merchandiseOptionCategories'
		));

	}



/**
 * Marks an existing Merchandise as deleted
 * @return [type] [description]
 */
	public function admin_delete($id = null) {

		// if no id, redirect back
		if(empty($id)) {
            $this->Session->setFlash(__('There was a problem deleting that merchandise.'), 'admin/notifications', array('type'=>'fail'));
			$this->redirect($this->referer());
		}

		$this->layout = false;

		// gets the to-be-deleted user
		$merchandiseToBeDeleted = $this->Merchandise->findById($id);

		// attempt to delete the user
		if($this->Merchandise->deleteMerchandise($id)) {
        	$this->Session->setFlash(__('The merchandise has been deleted.'), 'admin/notifications', array('type'=>'success'));

        } else {
        	$this->Session->setFlash(__('The merchandise could not be deleted.'), 'admin/notifications', array('type'=>'error'));
        }
		$this->redirect($this->referer());
	}

}