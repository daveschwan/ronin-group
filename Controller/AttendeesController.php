<?php
class AttendeesController extends AppController {




	public function beforeFilter() {
		$this->Security->unlockedActions = array('admin_add');
		parent::beforeFilter();
	}



// adds a new attendee to a course instance
	public function admin_add($courseInstanceId = null) {
		if(empty($courseInstanceId)) {
			$this->redirect($this->referer());
		}

		$courseInstance = $this->Attendee->CourseInstance->getCourseInstance($courseInstanceId, array('activeOnly' => false));
		if(empty($courseInstance)) {
			throw new NotFoundException();
		}

		if($this->request->is('post')) {

			// if they chose to add a user
			$userCreationFailed = false;
			$userCreated = true;
			if(isset($this->request->data['Attendee']['create_user']) && $this->request->data['Attendee']['create_user'] == 'true') {
				$this->request->data['User']['role_id'] = '18DD8261-AF42-4D16-B54C-E43808BB42F7'; // "User"
				$user = $this->Attendee->User->add($this->request->data['User']);
				if(empty($user['User']['id'])) {
					$userCreationFailed = true;
				}
				$this->request->data['Attendee']['user_id'] = $user['User']['id'];
				$this->request->data['Attendee']['name'] = $user['User']['name'];
			}

			// add the attendee
			if(!$userCreationFailed) {
				if(empty($this->request->data['Attendee']['name'])) {
					$this->request->data['Attendee']['name'] = $this->Attendee->User->findById($this->request->data['Attendee']['user_id'])['User']['name'];
				}
				$attendee = $this->Attendee->add($this->request->data['Attendee']);
				if($attendee) {
					$this->Session->setFlash('The Attendee has been added.', 'admin/notifications', array('type'=>'success'));
					$this->redirect(array('controller'=>'course_instances', 'action'=>'view', $courseInstanceId));
				} else {
					if(!empty($user['User']['id'])) {
						$this->Session->setFlash('The User was created, but the Attendee could not be added.', 'admin/notifications', array('type'=>'error'));
						$this->request->data['Attendee']['create_user'] = false;
						unset($this->request->data['User']);
						unset($this->request->data['Profile']);
					} else {
						$this->Session->setFlash('The Attendee could not be added.', 'admin/notifications', array('type'=>'error'));
					}
				}
			} else {
				$this->Session->setFlash('The User could not be created.  Please verify data and try again.', 'admin/notifications', array('type'=>'error'));
			}
		}
		$users = $this->Attendee->User->getUsers(array('activeOnly' => false, 'findType' => 'list'));

		$this->set(compact('courseInstance', 'users'));

	}


/**
 * Deletes an attendee
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
	public function admin_delete($id = null) {
		$courseInstanceId = $this->Attendee->courseInstanceId($id);
		$this->Attendee->deleteAttendee($id);
		$this->Session->setFlash(__('The attendee has been removed.'), 'admin/notifications', array('type'=>'success'));
		$this->redirect(array('controller'=>'course_instances', 'action'=>'view', $courseInstanceId));
	}

}