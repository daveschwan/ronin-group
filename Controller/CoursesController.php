<?php
class CoursesController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Security->unlockedActions = array('admin_edit'); // not idea, but can't get it to work any other way
	}


/**
 * List of all the courses
 * @return [type] [description]
 */
	public function admin_index() {
		$opts = array(
			'activeOnly' => false,
			'paginate'   => true,
			'limit'      => 30,
		);
		$this->Paginator->settings = $this->Course->getCourses($opts);
		$this->Paginator->settings['paramType'] = 'querystring';
		$this->Paginator->settings['order'] = array('Course.name'=>'asc');
		$courses = $this->paginate('Course');
		$this->set(compact('courses'));
	}



/**
 * ADMIN ONLY:
 * Creates a course
 */
	public function admin_add() {
		if($this->request->is('post')) {
			$course = $this->Course->add($this->request->data);
			if($course) {
				$this->Session->setFlash('The Course has been created.', 'admin/notifications', array('type'=>'success'));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash('The Course could not be created.', 'admin/notifications', array('type'=>'error'));
			}
		}
	}



/**
 * ADMIN ONLY:
 * Edit a course's info and permissions
 * id of the item to edit
 * @var [type]
 */
	public function admin_edit($id = null) {
		if(!$this->Course->exists($id)) {
			throw new NotFoundException('Could not find Course.');
		}
		if($this->request->is('put')) {
			if($this->Course->edit($this->request->data)) {
				$this->Session->setFlash('Changes to the course have been saved.', 'admin/notifications', array('type'=>'success'));
			} else {
				$this->Session->setFlash('Changes to the course could not be saved.', 'admin/notifications', array('type'=>'error'));
			}
		}
		$course = $this->Course->getCourse(array('id'=>$id, 'activeOnly' => false));
		$this->request->data = $course;

		// // gets the course instances for this course
		// $opts = array(
		// 	'activeOnly' => false,
		// 	'paginate'   => true,
		// 	'limit'      => 30,
		// );
		// $this->Paginator->settings = $this->Course->CourseInstance->getCourseInstances($id, $opts);
		// $this->Paginator->settings['paramType'] = 'querystring';
		// $this->Paginator->settings['order'] = array('CourseInstance.name'=>'asc');
		// $courseInstances = $this->paginate('CourseInstance');

		$this->set(compact('course')); //, 'courseInstances'
	}

/**
 * ADMIN ONLY:
 * View a course's info and permissions
 * id of the item to edit
 * @var [type]
 */
	public function admin_view($id = null) {
		if(!$this->Course->exists($id)) {
			throw new NotFoundException('Could not find Course.');
		}
		$course = $this->Course->getCourse(array('id'=>$id, 'activeOnly' => false));

		// gets the course instances for this course
		$opts = array(
			'activeOnly' => false,
			'paginate'   => true,
			'limit'      => 30,
		);
		$this->Paginator->settings = $this->Course->CourseInstance->getCourseInstances($id, $opts);
		$this->Paginator->settings['paramType'] = 'querystring';
		$this->Paginator->settings['order'] = array('CourseInstance.name'=>'asc');
		$courseInstances = $this->paginate('CourseInstance');

		$this->set(compact('course', 'courseInstances'));
	}


/**
 * Delete's a Course
 * @return [type] [description]
 */
	public function admin_delete($id = null) {
		$this->Course->deleteCourse($id, 'deleted');
		$this->Session->setFlash(__('The course has been deleted.'), 'admin/notifications', array('type'=>'success'));
		$this->redirect(array('action'=>'index', $id));
	}


}