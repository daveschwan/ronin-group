<?php

App::uses('Controller', 'Controller');

class AppController extends Controller {

	public $components = array(
		'Auth' => array(
			'loginAction' => array('controller' => 'users', 'action' => 'login', 'admin' => false),
			'loginRedirect' => array('controller' => 'dashboards', 'action' => 'index', 'admin' => true),
			'logoutRedirect' => '/',
			'authorize' => array('Controller'),
			'authenticate' => array(
				'Form' => array(
					'passwordHasher' => 'Blowfish',
					'fields' => array('username'=>'email', 'password'=>'password'),
					'scope' => array('User.active' => 1)
				)
			),
		),
		'Cookie',
		'DebugKit.Toolbar',
		'Paginator',
		'RequestHandler',
		'Security',
		'Session',
	);

	public $helpers = array(
		'Session',
		'Html',
		'Form' => array('className' => 'AppForm'),
		'Paginator',
		'Time',
	);



/**
 * Before Filter of the App Controller
 * @return [type] [description]
 */
	public function beforeFilter() {
		if($this->params['prefix'] == 'admin' && $this->name !== 'CakeError') {
			$this->layout = "admin";
		}

		//Security Settings
		$this->Security->blackHoleCallback = 'blackhole';

		// currently logged-in user
		$this->set('currentUser', $this->Auth->user());

		// settings
		$this->fetchSettings();
	}


/**
 * [isAuthorized description]
 * @param  [type]  $user [description]
 * @return boolean       [description]
 */
	public function isAuthorized($user) {

		// temporarily grant access to all
		return true;

		// give complete access to everyone for everything that's not 'admin' prefixed
		if($this->params['prefix'] !== 'admin') {
			return true;
		}

		// gives permission to Administrator
		if(isset($user['Role']['id']) && $user['Role']['name'] == 'Administrator') {
			return true;
		}

		//deny anything that wasn't handled
		return false;
	}



/**
 * Blackhole called by the Security component for a number of reasons
 * @return [type] [description]
 */
	public function blackhole($type) {
		switch($type) {
			case 'auth':
				debug("SECURITY:AUTH");
				exit;
				break;
			case 'csrf':
				debug("SECURITY:CSRF");
				exit;
				break;
			case 'get':
				debug("SECURITY:GET");
				exit;
				break;
			case 'post':
				debug("SECURITY:POST");
				exit;
				break;
			case 'put':
				debug("SECURITY:PUT");
				exit;
				break;
			case 'delete':
				debug("SECURITY:DELETE");
				exit;
				break;
			case 'secure':
				$this->redirect('https://' . env('SERVER_NAME') . $this->here);
				break;
		}
	}



	/**
	 * Read settings from DB and populate them in constants
	 */
	public function fetchSettings(){
		$this->loadModel('SettingKey');

		//Fetching All params
		$settings = $this->SettingKey->getSettings();

		foreach($settings as $settingsData) {
			$value = $settingsData['SettingKey']['default_value'];

			//note: can't check for !empty because some values are 0 (zero)
			if(isset($settingsData['Setting'][0]['value']) && $settingsData['Setting'][0]['value'] !== null && $settingsData['Setting'][0]['value'] !== '') {
				$value = $settingsData['Setting'][0]['value'];
			}
			// debug('Partner.'.$settingsData['PartnerSettingKey']['key'] . ' = ' . $value);
			Configure::write($settingsData['SettingKey']['key'], $value);
	   }
	}





}
