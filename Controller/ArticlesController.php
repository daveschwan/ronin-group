<?php
class ArticlesController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
	}


/**
 * List of all the articles
 * @return [type] [description]
 */
	public function admin_index() {
		$opts = array(
			'activeOnly' => false,
			'paginate'   => true,
			'limit'      => 30,
		);
		$this->Paginator->settings = $this->Article->getArticles($opts);
		$this->Paginator->settings['paramType'] = 'querystring';
		$this->Paginator->settings['order'] = array('Article.created'=>'asc');
		$articles = $this->paginate('Article');
		$this->set(compact('articles'));
	}



/**
 * ADMIN ONLY:
 * Creates a article
 */
	public function admin_add() {
		if($this->request->is('post')) {
			$article = $this->Article->add($this->request->data);
			if($article) {
				$this->Session->setFlash('The Article has been created.', 'admin/notifications', array('type'=>'success'));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash('The Article could not be created.', 'admin/notifications', array('type'=>'error'));
			}
		}
	}



/**
 * ADMIN ONLY:
 * Edit a article's info and permissions
 * id of the item to edit
 * @var [type]
 */
	public function admin_edit($id = null) {
		if(!$this->Article->exists($id)) {
			throw new NotFoundException('Could not find Article.');
		}
		if($this->request->is('put')) {
			if($this->Article->edit($this->request->data)) {
				$this->Session->setFlash('Changes to the Article have been saved.', 'admin/notifications', array('type'=>'success'));
			} else {
				$this->Session->setFlash('Changes to the Article could not be saved.', 'admin/notifications', array('type'=>'error'));
			}
		}
		$article = $this->Article->getArticle(array('id'=>$id, 'activeOnly' => false));
		$this->request->data = $article;

		$this->set(compact('article'));
	}



/**
 * Delete's an Article
 * @return [type] [description]
 */
	public function admin_delete($id = null) {
		$this->Article->deleteArticle($id, 'deleted');
		$this->Session->setFlash(__('The article has been deleted.'), 'admin/notifications', array('type'=>'success'));
		$this->redirect(array('action'=>'index', $id));
	}


}