<?php
class OrdersController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
	}


/**
 * List of all the orders
 * @return [type] [description]
 */
	public function admin_index() {
		$opts = array(
			'activeOnly' => false,
			'paginate'   => true,
			'limit'      => 30,
		);
		$this->Paginator->settings = $this->Order->getOrders($opts);
		$this->Paginator->settings['paramType'] = 'querystring';
		$this->Paginator->settings['order'] = array('Order.created'=>'asc');
		$orders = $this->paginate('Order');
		$this->set(compact('orders'));
	}


}