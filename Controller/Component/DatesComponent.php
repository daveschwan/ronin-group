<?php

App::uses('CakeTime', 'Utility');

class DatesComponent extends Component {


/**
 * Takes data with start and end dates and returns a summary of when it is
 * @param  [type] $instanceData [description]
 * @return [type]               [description]
 */
    public function dateTimeSummary($data, $options = array()) {
        $defaults = array(
            'dateFormat' => '%b. %e, %Y',
            'timeFormat' => '%H:%M %p' //
        );
        extract(array_merge($defaults, $options));

        $summary = '';
        if(empty($data['start'])) {
            return false;
        }
        $summary .= CakeTime::format($data['start'], $dateFormat);
        $summary .= ' from ';
        $summary .= ltrim(CakeTime::format($data['start'], $timeFormat), '0');
        $summary .= ' to ';
        $summary .= ltrim(CakeTime::format($data['end'], $timeFormat), '0');
        if(substr($data['start'], 0, 10) != substr($data['end'], 0, 10)) {
            $summary .= '. ' . CakeTime::format($data['end'], $dateFormat);
        }

        return $summary;
    }

}