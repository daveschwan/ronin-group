<?php
class UploaderComponent extends Component {

	//sets class defaults
	private $allowedExtensions = array(); //allow anything
	private $sizeLimit = 2097152; //bytes  2097152 = 2MB  (10485760 bytes = 10 MB)
	private $file;
	private $imageSizes = array();

	function __construct($controller, $settings) {

		//sets defaults
		if(empty($settings['sizeLimit'])) {
			$settings['sizeLimit'] = 10485760;
		}
		if(empty($settings['allowedExtensions'])) {
			$settings['allowedExtensions'] = array('jpeg', 'jpg', 'gif', 'png');
		}
		if(empty($settings['imageSizes'])) {
			$settings['imageSizes'] = array(
				'md' => array(600, 600, 'resize'),
				'sm' => array(350, 350, 'resize'),
				'default' => array(968, 600, 'resize'),
			);
		}

		$allowedExtensions = array_map("strtolower", $settings['allowedExtensions']);

		$this->allowedExtensions = $allowedExtensions;
		$this->sizeLimit = $settings['sizeLimit'];
		$this->imageSizes = $settings['imageSizes'];

		$this->checkServerSettings();

		if (isset($_GET['ezufile'])) {
			$this->file = new ezuUploadedFileXhr();
		} elseif (isset($_FILES['ezufile'])) {
			$this->file = new ezuUploadedFileForm();
		} else {
			$this->file = false;
		}
	}


/**
 * Checks to make sure the server settings are not limited to less than the specified $this->sizeLimit
 * @return doesn't return, but dies if it doesn't pass
 */
	private function checkServerSettings(){
		$postSize = $this->toBytes(ini_get('post_max_size'));
		$uploadSize = $this->toBytes(ini_get('upload_max_filesize'));

		if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
			$size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';
			die("{'error':'increase post_max_size and upload_max_filesize to $size'}");
		}
	}


	private function toBytes($str){
		$val = trim($str);
		$last = strtolower($str[strlen($str)-1]);
		switch($last) {
			case 'g': $val *= 1024;
			case 'm': $val *= 1024;
			case 'k': $val *= 1024;
		}
		return $val;
	}

	/**
	 * Returns array('success'=>true) or array('error'=>'error message')
	 */
	function handleUpload($uploadDirectory, $replaceOldFile = false){
		if (!is_writable($uploadDirectory)){
			return array('error' => "Server error. Upload directory isn't writable.");
		}

		if (!$this->file){
			return array('error' => 'No files were uploaded.');
		}

		$size = $this->file->getSize();

		if ($size == 0) {
			return array('error' => 'File is empty');
		}

		if ($size > $this->sizeLimit) {
			return array('error' => 'File is too large');
		}

		//sets the filename and extension
		$pathinfo = pathinfo($this->file->getName());
		$filename = $pathinfo['filename'];
		$filename = md5(uniqid());
		$ext = strtolower($pathinfo['extension']);

		// verify it's an allowed extension
		if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
			$these = implode(', ', $this->allowedExtensions);
			return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
		}

		// if (default) it's not supposed to replace a file with the same name, then incrememnt it's name
		if(!$replaceOldFile){
			/// don't overwrite previous files that were uploaded
			while (file_exists($uploadDirectory . $filename . '.' . $ext)) {
				$filename .= rand(10, 99);
			}
		}

		//upload initial file
		$save = $this->file->save($uploadDirectory . $filename . '.' . $ext);

		if(!$save) {
			return array('error'=> 'Could not save uploaded file.' . 'The upload was cancelled, or server error encountered');
		}

		$createdThumbs = true;
		if(!empty($this->imageSizes)) {
			foreach($this->imageSizes as $key => $size) {
				$dst_folder = WWW_ROOT . 'uploads' . DS . 'testing' . DS . 'test';
				$suffix = '';
				if($key != 'default') {
					$suffix = '_' . $key;
				}
				//function resize_image($cType = 'resize', $tmpfile, $dst_folder, $dstname = false, $newWidth = false, $newHeight = false, $quality = 75) {
				if(!$this->resize_image($size[2], $uploadDirectory.$filename.'.'.$ext, $uploadDirectory, $filename.$suffix.'.'.$ext, $size[0], $size[1], 80)) {
					$createdThumbs = false;
				}
			}
		}

		// if(!$createdThumbs) {
		//     return array('error'=> 'Could not create the thumbnails file.' . 'The upload was cancelled, or server error encountered');
		// }

		return array('success'=>$createdThumbs, 'filename'=>$filename.'.'.$ext, 'origFilename'=>$this->file->getName());

	}



	/**
	 * Deletes file, or image and associated thumbnail(s)
	 * e.g;
	 *  $this->Upload->delete_files(  ..??.., 'file_name.jpg');
	 *
	 * @param $filepath string The file to delete
	 * @param $filename string The file to delete
	 */
	function delete_files($filepath, $filename) {

		// Thumbnail copies
		foreach ($this->imageSizes as $suffix => $opts) {

			$pathinfo = pathinfo(WWW_ROOT . $filepath . DS . $filename);
			$tmpFilename = $pathinfo['filename'];
			$ext = $pathinfo['extension'];

			if($suffix != 'default') {
				$tmpFilename .= '_' . $suffix;
			}

			$photo = WWW_ROOT . $filepath . DS . $tmpFilename . '.' . $ext;
			if (is_file($photo)) {
				unlink($photo);
			}
		}

		/* Specified path */
		if (is_file(WWW_ROOT . $filepath . DS . $filename)) {
			unlink(WWW_ROOT . $filepath . DS . $filename);
		}
	}




	/*
	 * Creates resized image copy
	 *
	 * Parameters:
	 * cType: Conversion type {resize (default) | resizeCrop (square) | crop (from center)}
	 * tmpfile: original (tmp) file name
	 * newName: include extension (if desired)
	 * newWidth: the max width or crop width
	 * newHeight: the max height or crop height
	 * quality: the quality of the image
	 */

	function resize_image($cType = 'resize', $srcimg, $dst_folder, $dstname = false, $newWidth = false, $newHeight = false, $quality = 75) {

        ini_set('memory_limit', '256M');

		list($oldWidth, $oldHeight, $type) = getimagesize($srcimg);
		$ext = $this->image_type_to_extension($type);

		//if the src image is the same as the destination image, return false
		// if($srcimg == $dst_folder.$dstname) {
		//     return false;
		// }

		// If file is writeable, create destination (tmp) image
		if (is_writeable($dst_folder)) {
			$dstimg = $dst_folder . DS . $dstname;
		} else {
			// if dst_folder not writeable, let developer know
			debug('You must allow proper permissions for image processing. And the folder has to be writable.');
			debug("Run 'chmod 755 $dst_folder', and make sure the web server is it's owner.");
			return $this->log_cakephp_error_and_return('No write permissions on attachments folder.');
		}

		/* Check if something is requested, otherwise do not resize */
		if ($newWidth or $newHeight) {
			/* Delete tmp file if it exists */
			// if (file_exists($dstimg)) {
			//     unlink($dstimg);
			// } else {
				switch ($cType) {
					default:
					case 'resize':
						// Maintains the aspect ratio of the image and makes sure
						// that it fits within the maxW and maxH
						$widthScale = 2;
						$heightScale = 2;

						/* Check if we're overresizing (or set new scale) */
						if ($newWidth) {
							if ($newWidth > $oldWidth)
								$newWidth = $oldWidth;
							$widthScale = $newWidth / $oldWidth;
						}
						if ($newHeight) {
							if ($newHeight > $oldHeight)
								$newHeight = $oldHeight;
							$heightScale = $newHeight / $oldHeight;
						}
						if ($widthScale < $heightScale) {
							$maxWidth = $newWidth;
							$maxHeight = false;
						} elseif ($widthScale > $heightScale) {
							$maxHeight = $newHeight;
							$maxWidth = false;
						} else {
							$maxHeight = $newHeight;
							$maxWidth = $newWidth;
						}

						if ($maxWidth > $maxHeight) {
							$applyWidth = $maxWidth;
							$applyHeight = ($oldHeight * $applyWidth) / $oldWidth;
						} elseif ($maxHeight > $maxWidth) {
							$applyHeight = $maxHeight;
							$applyWidth = ($applyHeight * $oldWidth) / $oldHeight;
						} else {
							$applyWidth = $maxWidth;
							$applyHeight = $maxHeight;
						}
						$startX = 0;
						$startY = 0;
						break;

					case 'resizeCrop':
						/* Check if we're overresizing (or set new scale) */
						/* resize to max, then crop to center */
						if ($newWidth > $oldWidth)
							$newWidth = $oldWidth;
						$ratioX = $newWidth / $oldWidth;

						if ($newHeight > $oldHeight)
							$newHeight = $oldHeight;
						$ratioY = $newHeight / $oldHeight;

						if ($ratioX < $ratioY) {
							$startX = round(($oldWidth - ($newWidth / $ratioY)) / 2);
							$startY = 0;
							$oldWidth = round($newWidth / $ratioY);
							$oldHeight = $oldHeight;
						} else {
							$startX = 0;
							$startY = round(($oldHeight - ($newHeight / $ratioX)) / 2);
							$oldWidth = $oldWidth;
							$oldHeight = round($newHeight / $ratioX);
						}
						$applyWidth = $newWidth;
						$applyHeight = $newHeight;
						break;

					case 'crop':
						// straight centered crop
						$startY = ($oldHeight - $newHeight) / 2;
						$startX = ($oldWidth - $newWidth) / 2;
						$oldHeight = $newHeight;
						$applyHeight = $newHeight;
						$oldWidth = $newWidth;
						$applyWidth = $newWidth;
						break;
				}

				switch ($ext) {
					case 'gif' :
						$oldImage = imagecreatefromgif($srcimg);
						break;
					case 'png' :
						$oldImage = imagecreatefrompng($srcimg);
						break;
					case 'jpg' :
					case 'jpeg' :
						$oldImage = imagecreatefromjpeg($srcimg);
						break;
					default :
						// image type is not a possible option
						return false;
						break;
				}

				// Create new image
				$newImage = imagecreatetruecolor($applyWidth, $applyHeight);
				// Put old image on top of new image
				imagealphablending($newImage, false);
				imagesavealpha($newImage, true);
				imagecopyresampled($newImage, $oldImage, 0, 0, $startX, $startY, $applyWidth, $applyHeight, $oldWidth, $oldHeight);

				switch ($ext) {
					case 'gif' :
						imagegif($newImage, $dstimg, $quality);
						break;
					case 'png' :
						imagepng($newImage, $dstimg, round($quality / 10));
						break;
					case 'jpg' :
					case 'jpeg' :
						imagejpeg($newImage, $dstimg, $quality);
						break;
					default :
						return false;
						break;
				}

				imagedestroy($newImage);
				imagedestroy($oldImage);
				return true;
			// }
		} else { /* Nothing requested */
			return false;
		}
	}


	/* Many helper functions */

	function copy_or_log_error($tmp_name, $dst_folder, $dst_filename) {
		if (is_writeable($dst_folder)) {
			if (!copy($tmp_name, $dst_folder . DS . $dst_filename)) {
				unset($dst_filename);
				return $this->log_cakephp_error_and_return('Error uploading file.', 'publicaciones');
			}
		} else {
			// if dst_folder not writeable, let developer know
			debug('You must allow proper permissions for image processing. And the folder has to be writable.');
			debug("Run 'chmod 755 $dst_folder', and make sure the web server is it's owner.");
			return $this->log_cakephp_error_and_return('No write permissions on attachments folder.');
		}
	}

	function is_image($file_type) {
		return in_array(strtolower($file_type), $this->config['allowed_image_file_types']);
	}

	function log_proper_error($err_code) {
		switch ($err_code) {
			case UPLOAD_ERR_NO_FILE:
				return 0;
			case UPLOAD_ERR_INI_SIZE:
				$e = 'The uploaded file exceeds the upload_max_filesize directive in php.ini.';
				break;
			case UPLOAD_ERR_FORM_SIZE:
				$e = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.';
				break;
			case UPLOAD_ERR_PARTIAL:
				$e = 'The uploaded file was only partially uploaded.';
				break;
			case UPLOAD_ERR_NO_TMP_DIR:
				$e = 'Missing a temporary folder.';
				break;
			case UPLOAD_ERR_CANT_WRITE:
				$e = 'Failed to write file to disk.';
				break;
			case UPLOAD_ERR_EXTENSION:
				$e = 'File upload stopped by extension.';
				break;
			default:
				$e = 'Unknown upload error. Did you add array(\'type\' => \'file\') to your form?';
		}
		return $this->log_cakephp_error_and_return($e);
	}

	function log_cakephp_error_and_return($msg) {
		$_error["{$this->config['default_col']}_file_name"] = $msg;
		$this->controller->{$this->controller->modelClass}->validationErrors = array_merge($_error, $this->controller->{$this->controller->modelClass}->validationErrors);
		$this->log($msg, 'attachment-component');
		return false;
	}

	function image_type_to_extension($imagetype) {
		if (empty($imagetype))
			return false;
		switch ($imagetype) {
			case IMAGETYPE_TIFF_II : return 'tiff';
			case IMAGETYPE_TIFF_MM : return 'tiff';
			case IMAGETYPE_JPEG : return 'jpg';
			case IMAGETYPE_WBMP : return 'wbmp';
			case IMAGETYPE_GIF : return 'gif';
			case IMAGETYPE_PNG : return 'png';
			case IMAGETYPE_SWF : return 'swf';
			case IMAGETYPE_PSD : return 'psd';
			case IMAGETYPE_BMP : return 'bmp';
			case IMAGETYPE_JPC : return 'jpc';
			case IMAGETYPE_JP2 : return 'jp2';
			case IMAGETYPE_JPX : return 'jpf';
			case IMAGETYPE_JB2 : return 'jb2';
			case IMAGETYPE_SWC : return 'swc';
			case IMAGETYPE_IFF : return 'aiff';
			case IMAGETYPE_XBM : return 'xbm';
			default : return false;
		}
	}

	function format_bytes($size) {
		$units = array(' B', ' KB', ' MB', ' GB', ' TB');
		for ($i = 0; $size >= 1024 && $i < 4; $i++)
			$size /= 1024;
		return round($size, 2) . $units[$i];
	}


}




/**
 * Handle file uploads via XMLHttpRequest
 */
class ezuUploadedFileXhr {
	/**
	 * Save the file to the specified path
	 * @return boolean TRUE on success
	 */
	function save($path) {
		$input = fopen("php://input", "r");
		$temp = tmpfile();
		$realSize = stream_copy_to_stream($input, $temp);
		fclose($input);

		if ($realSize != $this->getSize()){
			return false;
		}

		$target = fopen($path, "w");
		fseek($temp, 0, SEEK_SET);
		stream_copy_to_stream($temp, $target);
		fclose($target);

		return true;
	}
	function getName() {
		return $_GET['ezufile'];
	}
	function getSize() {
		if (isset($_SERVER["CONTENT_LENGTH"])){
			return (int)$_SERVER["CONTENT_LENGTH"];
		} else {
			throw new Exception('Getting content length is not supported.');
		}
	}
}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class ezuUploadedFileForm {
	/**
	 * Save the file to the specified path
	 * @return boolean TRUE on success
	 */
	function save($path) {
		if(!move_uploaded_file($_FILES['ezufile']['tmp_name'], $path)){
			return false;
		}
		return true;
	}
	function getName() {
		return $_FILES['ezufile']['name'];
	}
	function getSize() {
		return $_FILES['ezufile']['size'];
	}
}