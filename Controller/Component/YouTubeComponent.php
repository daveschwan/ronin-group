<?php
class YouTubeComponent extends Component {


    // returns the info from a youtube video
    public function info($urlOrId) {

        $id = $this->getId($urlOrId);

         // set video data feed URL
        $feedURL = 'http://gdata.youtube.com/feeds/api/videos/' . $id;

        // read feed into SimpleXML object
        $entry = @simplexml_load_file($feedURL);

        // parse video entry
        $video = $this->parseVideoEntry($entry);
        $details = array();
        if($video) {
            $details['title'] = (string)$video->title;
            $details['description'] = (string)$video->description;
            $details['watch_url'] = (string)$video->watchURL;
            $details['thumbnail_url'] = (string)$video->thumbnailURL;
            $details['length'] = (string)$video->length;
            $details['rating'] = (string)$video->rating;
            $details['comments_url'] = (string)$video->commentsURL;
            $details['comments_count'] = (string)$video->commentsCount;
            $details['youtube_id'] = (string)$id;
        }

        //title, description, watchURL, thumbnailURL, length(seconds), rating, commentsURL, commentsCount
        return $details;
    }


    /**
     * takes an entire YouTube URL or a YouTube id and returns just the id
     * does not take into account if they paste anything other than the exact id or the full url
     * @param  [type] $string [description]
     * @return [type]         [description]
     */
    public function getId($string) {
        $separator = null;
        if(strpos($string, '?v=')) {
            $separator = '?v=';
        }
        if(strpos($string, '&v=')) {
            $separator = '&v=';
        }
        if(!empty($separator)) {
            try {
                $parts = explode($separator, $string);
                $parts = explode('&', $parts[1]);
                $string = $parts[0];
            } catch (Exception $e) {
                return null;
            }
        }
        return $string;
    }


    /**
     * actually parses the xml data
     * @param  [type] $entry [description]
     * @return [type]        [description]
     */
    public function parseVideoEntry($entry) {
        $obj= new stdClass;

        if(!is_object($entry)) {
            return false;
        }

        // get nodes in media: namespace for media information
        $media = $entry->children('http://search.yahoo.com/mrss/');
        $obj->title = $media->group->title;
        $obj->description = $media->group->description;

        // get video player URL
        $attrs = $media->group->player->attributes();
        $obj->watchURL = $attrs['url'];

        // get video thumbnail
        $attrs = $media->group->thumbnail[0]->attributes();
        $obj->thumbnailURL = $attrs['url'];

        // get <yt:duration> node for video length
        $yt = $media->children('http://gdata.youtube.com/schemas/2007');
        $attrs = $yt->duration->attributes();
        $obj->length = $attrs['seconds'];

        // get <yt:stats> node for viewer statistics
        $yt = $entry->children('http://gdata.youtube.com/schemas/2007');
        $attrs = $yt->statistics->attributes();
        $obj->viewCount = $attrs['viewCount'];

        // get <gd:rating> node for video ratings
        $gd = $entry->children('http://schemas.google.com/g/2005');
        if ($gd->rating) {
            $attrs = $gd->rating->attributes();
            $obj->rating = $attrs['average'];
        } else {
            $obj->rating = 0;
        }

        // get <gd:comments> node for video comments
        $gd = $entry->children('http://schemas.google.com/g/2005');
        if ($gd->comments->feedLink) {
            $attrs = $gd->comments->feedLink->attributes();
            $obj->commentsURL = $attrs['href'];
            $obj->commentsCount = $attrs['countHint'];
        }

        // get feed URL for video responses
        $entry->registerXPathNamespace('feed', 'http://www.w3.org/2005/Atom');
        $nodeset = $entry->xpath("feed:link[@rel='http://gdata.youtube.com/schemas/
        2007#video.responses']");
        if (count($nodeset) > 0) {
            $obj->responsesURL = $nodeset[0]['href'];
        }

        // get feed URL for related videos
        $entry->registerXPathNamespace('feed', 'http://www.w3.org/2005/Atom');
        $nodeset = $entry->xpath("feed:link[@rel='http://gdata.youtube.com/schemas/
        2007#video.related']");
        if (count($nodeset) > 0) {
            $obj->relatedURL = $nodeset[0]['href'];
        }

        // return object to caller
        return $obj;
    }

}