<?php

App::uses('Component', 'Controller');
App::uses('CakeConfigure', 'Utility');
class AuthNetComponent extends Component {


/*
	//for live
	"x_login"			=> "496q6EeVs7Lt",
	"x_tran_key"		=> "7C86AbG36gL8px44",
	post url:  "https://secure.authorize.net/gateway/transact.dll"

	//for testing
	"x_login"			=> "4HVv4P92nat",
	"x_tran_key"		=> "5Cg336pM2p6HtFcf",
	post url:  "https://test.authorize.net/gateway/transact.dll"
*/

	//sets class defaults (OUR LIVE DATA)
    private $postURL = 'https://secure.authorize.net/gateway/transact.dll'; // post url
    private $tranKey = '7C86AbG36gL8px44'; // api transaction key
    private $login = '496q6EeVs7Lt'; // api login

    public $components = array('Session');



/**
 * Overrides the default connection data with partner-specified data (if it's not empty)
 * @param [type] $controller [description]
 * @param [type] $settings   [description]
 */
    private function getSettings() {

		$partnerAuthInfo = Configure::read('Partner.Authnet');

		// overrides the defaults with partner-specified authorize.net data
		if(!empty($partnerAuthInfo['login'])) {
			$this->login = $partnerAuthInfo['login'];
		}
		if(!empty($partnerAuthInfo['tran_key'])) {
			$this->tranKey = $partnerAuthInfo['tran_key'];
		}
		if(!empty($partnerAuthInfo['post_url'])) {
			$this->postURL = $partnerAuthInfo['post_url'];
		}
    }



/**
 * Runs an authorization on the credit card
 * if it passes, it stores the authentication for future batch processing
 * @param  [type] $data     [description]
 * @param  [type] $complete [description]
 * @return [type]           [description]
 */
	public function auth_capture($data, $complete) {

		$this->getSettings();

		// debug($this->login);
		// debug($this->tranKey);
		// debug($this->postURL);

		/*Define where to post this request to */

		$data['Order']['cc_expire_year'] = substr($data['Order']['cc_expire_year'], 2, 2);

		$post_values = array(
			//use this for testing
			"x_login"			=> $this->login,
			"x_tran_key"		=> $this->tranKey,

			"x_version"			=> "3.1",
			"x_delim_data"		=> "TRUE",
			"x_delim_char"		=> "|",
			"x_relay_response"	=> "TRUE",

			"x_type"			=> "AUTH_CAPTURE",

			"x_method"			=> "CC",
			"x_card_num"		=> $data['Order']['cc_number'],
			"x_exp_date"		=> $data['Order']['cc_expire_month'].$data['Order']['cc_expire_year'],
			"x_card_code"		=> $data['Order']['cc_cvv'],

			"x_amount"			=> $complete['Order']['total_paid'],
			"x_description"		=> Configure::read('Partner.name')." ".$complete['EventInstance']['Event']['name']." Ticket Purchase",

			"x_first_name"		=> $complete['Order']['first_name'],
			"x_last_name"		=> $complete['Order']['last_name'],
			"x_address"			=> $complete['Order']['address'],
			"x_state"			=> $complete['Order']['state'],
			"x_zip"				=> $complete['Order']['postal_code'],
			"x_customer_ip"		=> $_SERVER['REMOTE_ADDR'],
			"x_invoice_num"		=> $complete['Order']['id'],
			"x_tax"				=> '0.00',
			"x_duplicate_window"=>"0",

			// Additional fields can be added here as outlined in the AIM integration
			// guide at: http://developer.authorize.net
		);

		//debug($post_values);

		// This section takes the input fields and converts them to the proper format
		// for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
		$post_string = "";
		foreach( $post_values as $key => $value )
			{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
		$post_string = rtrim( $post_string, "& " );

		$response_array = $this->auth_post($this->postURL, $post_values);
		return $response_array;
	}

	public function auth_credit($data){

		$this->getSettings();

		/*Define where to post this request to */

		$post_values = array(
			//use this for testing
			"x_login"			=> $this->login,
			"x_tran_key"		=> $this->tranKey,

			"x_version"			=> "3.1",
			"x_delim_data"		=> "TRUE",
			"x_delim_char"		=> "|",
			"x_relay_response"	=> "TRUE",

			'x_type'			=> "CREDIT",
			'x_trans_id'		=> $data['x_trans_id'],
			'x_card_num'		=> $data['x_card_num'],
			'x_amount'			=> $data['x_amount'],
			'x_duplicate_window'=>"0",

			// Additional fields can be added here as outlined in the AIM integration
			// guide at: http://developer.authorize.net
		);
		//debug($post_values);

		$response_array = $this->auth_post($this->postURL, $post_values);
		return $response_array;
	}

	public function auth_void($data) {

		$this->getSettings();

		/*Define where to post this request to */

		$post_values = array(
			//use this for testing
			"x_login"			=> $this->login,
			"x_tran_key"		=> $this->tranKey,

			"x_version"			=> "3.1",
			"x_delim_data"		=> "TRUE",
			"x_delim_char"		=> "|",
			"x_relay_response"	=> "TRUE",

			'x_type'			=> "VOID",
			'x_trans_id'		=> $data['x_trans_id'],
			'x_card_num'		=> $data['x_card_num'],
			'x_amount'			=> $data['x_amount'],

			// Additional fields can be added here as outlined in the AIM integration
			// guide at: http://developer.authorize.net
		);
		//debug($post_values);

		$response_array = $this->auth_post($this->postURL, $post_values);
		return $response_array;
	}

	public function auth_post($post_url, $post_values){
		// This section takes the input fields and converts them to the proper format
		// for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
		$post_string = "";
		foreach( $post_values as $key => $value )
			{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
		$post_string = rtrim( $post_string, "& " );

		// If you receive an error, you may want to ensure that you have the curl
		// library enabled in your php configuration
		$request = curl_init($post_url); // initiate curl object
			curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
			curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
			curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
			$post_response = curl_exec($request); // execute curl post and store results in $post_response
			// additional options may be required depending upon your server configuration
			// you can find documentation on curl options at http://www.php.net/curl_setopt
		curl_close ($request); // close curl object

		// This line takes the response and breaks it into an array using the specified delimiting character
		$response_array = explode($post_values["x_delim_char"],$post_response);
		return $response_array;
	}
}
