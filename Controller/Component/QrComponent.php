<?php

class QrComponent extends Component {


/**
 * [get description]
 * @param  string $string  [description]
 * @param  string $path    string (ending with slash) for the folder within basePath where the QR code should be stored (before folderDepth is generated)
 * @param  array  $options [description]
 * @return [type]          [description]
 */
	public function get($string = '', $folder = '', $options = array()) {
		if(empty($string)) return '';
		$string = $this->cleanString($string);
		$defaults = array(
			//'basePath' => ROOT . DS . APP_DIR . DS . 'qr_codes/',
			'basePath' => 'img/qr/',
			'encoding' => 'UTF-8',
			'width' => '300',
			'height' => '300',
			'errorCorrectionLevel' => 'H',
			'margin' => '0',
			'folderDepth' => 3,
			'imgExtension' => '.png'
		);
		extract(array_merge($defaults, $options), EXTR_OVERWRITE);
		$path = $basePath . $folder;
		if(!file_exists($path)) {
			mkdir($path);
		}
		for($i=1; $i<=$folderDepth; $i++) {
			$folder = rand(0,99) . '/';
			if(!file_exists($path . $folder)) {
				mkdir($path . $folder);
			}
			$path .= $folder;
		}
		$url = 'https://chart.googleapis.com/chart?cht=qr&chl='.$string.'&choe='.$encoding.'&chs='.$width.'x'.$height.'&chld='.$errorCorrectionLevel.'|'.$margin;
		if(copy($url, APP . WEBROOT_DIR . DS . $path . $string . $imgExtension)) {
			return ltrim($path, 'img/') . $string . $imgExtension;
		}
		return '';
	}



/**
 * Takes a string and removes all non alpha-numeric and space characters (except hyphens and underscores) and truncates
 * @param  [type] $string [description]
 * @return [type]         [description]
 */
	public function cleanString($string, $options = array()) {
		if(empty($string)) return '';
		$defaults = array(
			'maxLength' => 60
		);
		extract(array_merge($defaults, $options), EXTR_OVERWRITE);
		$string = preg_replace("/[^A-Za-z0-9-_]/", "", $string);
		$string = substr($string, 0, $maxLength);
		return $string;
	}
}